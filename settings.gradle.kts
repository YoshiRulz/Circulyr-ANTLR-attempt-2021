rootProject.name = "circulyr-parser"
include("antlr-codegen")

pluginManagement {
	repositories {
		mavenLocal()
		gradlePluginPortal()
		maven("https://jitpack.io")
	}
	plugins {
		kotlin("multiplatform") version "1.5.30"
	}
}
dependencyResolutionManagement {
	repositories {
		mavenLocal()
		mavenCentral()
		maven("https://jitpack.io")
	}
}
