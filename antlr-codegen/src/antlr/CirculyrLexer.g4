lexer grammar CirculyrLexer;

fragment BARE_WORD_IN_LYR_CHAR: [\p{L}"',\-;?—]; // Unicode letters and some punctuation (sadly, the latter needs to be hardcoded, as there's no mechanism to exclude chars from the set)
fragment DIGIT: [0-9];
fragment DIGIT_NZ: [1-9];
fragment SPACE_CHAR: [ \u3000]; // ASCII space or CJK fullwidth space
fragment TAG_CLOSE_CHAR: ']';

TAG_OPEN: '[' -> pushMode(IN_TAG);

NEWLINE: '\n' | '\r\n';
SPACE: SPACE_CHAR;
ALT_V_LYRIC_OPEN: '(';
ALT_V_LYRIC_CLOSE: ')';
ENJAMBMENT_MARKER: '/'; // "virgule"
CONTINUATION_MARKER: '...'; // "ellipsis"

BARE_WORD: '&'+ | BARE_WORD_IN_LYR_CHAR+; // either "word" of N indistinguishable syllables (ampersands), or an actual word

mode IN_TAG;

RTAG_CLOSE: TAG_CLOSE_CHAR -> popMode;
TAG_TYPE_SEPARATOR: ':' -> mode(IN_LTAG);

RTAG_TYPE: 'x';
REP_COUNT: DIGIT_NZ DIGIT*;
LTAG_TYPE: [a-z] [a-z] [a-z]?;

mode IN_LTAG;

LTAG_CLOSE: TAG_CLOSE_CHAR -> popMode;
LTAG_FIELD_SEPARATOR: '|';
SPACE_IN_LTAG: SPACE_CHAR;

BARE_WORD_IN_LTAG: (~[ \]|])+; // see BARE_WORD_IN_LYR_CHAR, but rather than list a bunch of Unicode classes we use a negated set
