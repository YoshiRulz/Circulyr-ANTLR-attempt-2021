parser grammar CirculyrParser;

options { tokenVocab=CirculyrLexer; }

circulyrFile: paragraph (NEWLINE paragraph)*;
paragraph: line+;
line: (rTag | lTag | (lTag SPACE)? lyricWithoutTags (SPACE rTag)? (SPACE lTag)?) NEWLINE;

lyricWithoutTags: altVLyric | (altVLyric SPACE)? mainVLyric (SPACE altVLyric)?;
altVLyric: ALT_V_LYRIC_OPEN bareWordsInLyric ALT_V_LYRIC_CLOSE;
mainVLyric: bareWordsInLyric;
bareWordsInLyric: BARE_WORD (SPACE BARE_WORD)* contMark=anyContMarker?;
anyContMarker: CONTINUATION_MARKER | SPACE ENJAMBMENT_MARKER;

rTag: TAG_OPEN RTAG_TYPE count=REP_COUNT RTAG_CLOSE;
lTag: TAG_OPEN type=LTAG_TYPE TAG_TYPE_SEPARATOR (SPACE_IN_LTAG firstField=bareWordsInLTag extraFields=lTagExtraFields?)? LTAG_CLOSE;
lTagExtraFields: (SPACE_IN_LTAG LTAG_FIELD_SEPARATOR SPACE_IN_LTAG bareWordsInLTag)+;
bareWordsInLTag: BARE_WORD_IN_LTAG (SPACE_IN_LTAG BARE_WORD_IN_LTAG)*;
