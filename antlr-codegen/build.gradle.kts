@file:Suppress("SpellCheckingInspection", "UNUSED_VARIABLE")

import com.strumenta.antlrkotlin.gradleplugin.AntlrKotlinTask
import java.io.*

buildscript {
	// we have to re-declare these here :(

	fun depStr(partialCoords: String, version: String) = "$partialCoords:$version"

	fun ktANTLR(module: String) = depStr("com.strumenta.antlr-kotlin:antlr-kotlin-$module", "6304d5c1c4")

	dependencies {
		classpath(ktANTLR("gradle-plugin"))
	}
}

plugins {
	kotlin("multiplatform")
}

fun depStr(partialCoords: String, version: String) = "$partialCoords:$version"

fun ktANTLR(module: String) = depStr("com.strumenta.antlr-kotlin:antlr-kotlin-$module", "6304d5c1c4")

kotlin {
	js(IR) {
		browser()
	}

	jvm {
		val javaTargetVersion = JavaLanguageVersion.of(16)
		jvmToolchain {
			(this as JavaToolchainSpec).languageVersion.set(javaTargetVersion)
		}
		compilations.all {
			kotlinOptions {
				jvmTarget = javaTargetVersion.toString()
			}
		}
	}

//	linuxX64()

	sourceSets {
		val commonMain by getting {
			kotlin.srcDir("build/codegen/commonMain")
			dependencies {
				api(ktANTLR("runtime"))
			}
		}

		val jsMain by getting {
			this.dependsOn(commonMain)
		}

		val jvmMain by getting {
			this.dependsOn(commonMain)
		}

//		val linuxX64jvmMain by getting {
//			this.dependsOn(commonMain)
//		}
	}
}

val generateKotlinCommonGrammarSource by tasks.registering(AntlrKotlinTask::class) {
	antlrClasspath = configurations.detachedConfiguration(project.dependencies.create(ktANTLR("target")))
	maxHeapSize = "64m"
	packageName = "club.circulyr.parser"
	arguments = listOf("-no-visitor", "-no-listener")
	source = project.objects.sourceDirectorySet("antlr", "antlr").srcDir("src/antlr").apply {
		include("*.g4")
	}
	outputDirectory = File("build/codegen/temp")
}

class KotlinSuppressionInjectorFilterReader(`in`: Reader): FilterReader(StringReader(SUPPRESS_ANNOT_HEADER + `in`.readText())) { // dumb, probably inefficient way to prepend to file
	companion object {
		private val SUPPRESS_ANNOT_HEADER = arrayOf(
			"PARAMETER_NAME_CHANGED_ON_OVERRIDE",
			"SENSELESS_COMPARISON",
			"UNCHECKED_CAST",
			"UNUSED_PARAMETER",
			"UNUSED_VALUE",
			"USELESS_CAST",
			"VARIABLE_WITH_REDUNDANT_INITIALIZER"
		).joinToString(separator = ", ", prefix = "@file:Suppress(", postfix = ")\n\n") { "\"$it\"" }
	}
}

val postGenerateSource by tasks.registering(Copy::class) {
	dependsOn(generateKotlinCommonGrammarSource)
	from("build/codegen/temp")
	include("**/*.kt")
	filter<KotlinSuppressionInjectorFilterReader>()
	into("build/codegen/commonMain")
}

listOf("compileKotlinJs", "compileKotlinJvm", "compileKotlinMetadata").forEach {
	tasks[it].dependsOn(postGenerateSource)
}
