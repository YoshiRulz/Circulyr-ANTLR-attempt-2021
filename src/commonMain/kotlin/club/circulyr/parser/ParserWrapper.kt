package club.circulyr.parser

import club.circulyr.parser.CirculyrParser.*
import club.circulyr.parser.HighLevelParser.LTagLocation.*
import org.antlr.v4.kotlinruntime.CharStreams
import org.antlr.v4.kotlinruntime.CommonTokenStream
import org.antlr.v4.kotlinruntime.Token
import org.antlr.v4.kotlinruntime.tree.ParseTree
import org.antlr.v4.kotlinruntime.tree.TerminalNode

private inline val String.asQuotedString: String
	get() = "\"$this\""

private inline val String.increaseIndent: String
	get() = this.replace("\n", "\n\t")

private inline val String?.orNullLiteral: String
	get() = this ?: "null"

private val ParseTree.isNewlineToken: Boolean
	get() = this.isToken(Tokens.NEWLINE)

internal val ParseTree.isSpaceToken: Boolean
	get() = this.isToken(Tokens.SPACE)

internal fun ParseTree.isToken(type: Tokens): Boolean
	= this is TerminalNode && this.payload.let { it is Token && it.type == type.id }

private fun <E> MutableList<E>.removeAt(range: IntRange): List<E> {
	require(range.first in this.indices && range.last in this.indices)
	val removed = this.subList(range.first, range.last + 1).toList()
	for (i in range) this.removeAt(0)
	return removed
}

internal fun BareWordsInLyricContext.parseTextAndContMarker(): Pair<String, ContinuationMarkOption>
	= this.contMark?.let {
		val contTokenNode = it.children!!.last()
		when {
			contTokenNode.isToken(Tokens.CONTINUATION_MARKER) -> Pair(this.text.dropLast(3), ContinuationMarkOption.Continuation)
			contTokenNode.isToken(Tokens.ENJAMBMENT_MARKER) -> Pair(this.text.dropLast(2), ContinuationMarkOption.Enjambment)
			else -> error("unexpected token")
		}
	} ?: Pair(this.text, ContinuationMarkOption.None)

public enum class ContinuationMarkOption {
	None, Continuation, Enjambment;

	public companion object
}

public object LowLevelParser {
	private fun parseLine(line: ParseTree): List<ParseTree>
		= buildList {
			val lastIndex = (line as LineContext).childCount - 1
			line.children!!.forEachIndexed { i, branch ->
				// add only the even-indexed nodes (the tags and lyrics), omitting the spaces separating them and the trailing newline
				// note LyricWithoutTagsContext nodes may have multiple children, in which case there will be terminal nodes (spaces) separating them!
				if (i % 2 == 0) add(branch)
				else check(if (i == lastIndex) branch.isNewlineToken else branch.isSpaceToken)
			}
		}

	private fun parseParagraph(paragraph: ParseTree): List<List<ParseTree>>
		= (paragraph as ParagraphContext).children!!.map(::parseLine)

	public fun parse(s: String): List<List<List<ParseTree>>>
		= buildList {
			CirculyrParser(CommonTokenStream(CirculyrLexer(CharStreams.fromString(s))))
				.circulyrFile().children!!
				.forEachIndexed { i, branch ->
					// add only the even-indexed nodes (the paragraphs), omitting the empty lines separating them
					if (i % 2 == 0) add(parseParagraph(branch))
					else check(branch.isNewlineToken)
				}
		}
}

public sealed interface HighLevelBranchOrLeaf {
	public fun reconstructSource(): String

	/** verbose, unambiguous serialisation of AST */
	abstract override fun toString(): String

	public companion object
}

public sealed interface HighLevelBranch: HighLevelBranchOrLeaf {
	public val children: List<HighLevelBranchOrLeaf>

	public companion object
}

public sealed interface LineOrSequence: HighLevelBranchOrLeaf {
	/** in source i.e. before unrolling refs/reps */
	public val lineList: List<Line>

	public companion object
}

public sealed interface Line: LineOrSequence {
	override val lineList: List<Line>
		get() = listOf(this)

	public companion object
}

public object ParagraphBreakMarker: Line {
	override fun reconstructSource(): String
		= ""

	override fun toString(): String
		= ""
}

public data class RTag(public val repCount: UInt): Line {
	override fun reconstructSource(): String
		= "[x$repCount]"

	override fun toString(): String
		= "RTag($repCount)"

	public companion object
}

public open class LTag(public val tagType: String, public val fields: List<String>): Line {
	public constructor(tagType: String, vararg fields: String): this(tagType, fields.toList())

	override fun equals(other: Any?): Boolean
		= other is LTag
			&& other.tagType == tagType
			&& other.fields.size == fields.size
			&& other.fields.withIndex().all { (i, s) -> fields[i] == s }

	override fun hashCode(): Int
		= tagType.hashCode() * 31 + fields.hashCode()

	override fun reconstructSource(): String
		= "[$tagType${if (fields.isEmpty()) ":" else ": "}${fields.joinToString(" | ")}]"

	override fun toString(): String
		= "LTag(${tagType.asQuotedString}${fields.joinToString("") { ", ${it.asQuotedString}" }})"

	public companion object
}

@Suppress("EqualsOrHashCode")
public class ReferenceTag(public val identifier: String): LTag(HighLevelParser.TAG_TYPE_REF, listOf(identifier)) {
	override fun equals(other: Any?): Boolean
		= other is ReferenceTag && identifier == other.identifier

	public companion object
}

@Suppress("EqualsOrHashCode")
public class SnippetReferenceTag(public val identifier: String): LTag(HighLevelParser.TAG_TYPE_SNIPPET_REF, listOf(identifier)) {
	override fun equals(other: Any?): Boolean
		= other is SnippetReferenceTag && identifier == other.identifier

	public companion object
}

public sealed interface LineWithoutLTags: Line {
	public companion object
}

public sealed interface LyricWithoutTags: LineWithoutLTags {
	public companion object
}

public data class Lyric(public val text: String, public val continuationMark: ContinuationMarkOption = ContinuationMarkOption.None): LyricWithoutTags {
	override fun reconstructSource(): String
		= text + when (continuationMark) {
			ContinuationMarkOption.Enjambment -> " /"
			ContinuationMarkOption.Continuation -> "..."
			else -> ""
		}

	override fun toString(): String
		= "Lyric(${toStringInner()})"

	internal fun toStringInner(): String
		= "${text.asQuotedString}, $continuationMark"
}

public data class CombinedLyric(public val prependedAltVoice: Lyric?, public val mainVoice: Lyric, public val appendedAltVoice: Lyric? = null): LyricWithoutTags {
	init {
		require(prependedAltVoice != null || appendedAltVoice != null)
	}

	override fun reconstructSource(): String
		= "${prependedAltVoice?.reconstructSource()?.let { "($it) " } ?: ""}${mainVoice.reconstructSource()}${appendedAltVoice?.reconstructSource()?.let { " ($it)" } ?: ""}"

	override fun toString(): String
		= "CombinedLyric(\n\t${prependedAltVoice?.toString().orNullLiteral},\n\t$mainVoice,\n\t${appendedAltVoice?.toString().orNullLiteral}\n)"

	public companion object
}

public data class AltVoiceOnlyLyric(public val lyric: Lyric): LyricWithoutTags {
	public constructor(text: String, continuationMark: ContinuationMarkOption = ContinuationMarkOption.None): this(Lyric(text, continuationMark))

	override fun reconstructSource(): String
		= "(${lyric.reconstructSource()})"

	override fun toString(): String
		= "AltVoiceOnlyLyric(${lyric.toStringInner()})"

	public companion object
}

public data class RepeatedLine(public val repCount: UInt, public val line: LyricWithoutTags): LineWithoutLTags {
	override fun reconstructSource(): String
		= "${line.reconstructSource()} [x$repCount]"

	override fun toString(): String
		= "RepeatedLine(\n\t$repCount,\n\t${line.toString().increaseIndent}\n)"

	public companion object
}

public data class TaggedLine(public val prependedTag: LTag?, public val line: LineWithoutLTags, public val appendedTag: LTag? = null): Line {
	public constructor(line: LineWithoutLTags, appendedTag: LTag): this(prependedTag = null, line, appendedTag)

	init {
		require(prependedTag != null || appendedTag != null)
	}

	override fun reconstructSource(): String
		= "${prependedTag?.reconstructSource()?.let { "$it " } ?: ""}${line.reconstructSource()}${appendedTag?.reconstructSource()?.let { " $it" } ?: ""}"

	override fun toString(): String
		= "TaggedLine(\n\t${prependedTag?.toString()?.increaseIndent.orNullLiteral},\n\t${line.toString().increaseIndent},\n\t${appendedTag?.toString()?.increaseIndent.orNullLiteral}\n)"

	public companion object
}

public fun TaggedLine.withoutAppendedTag(): Line
	= if (this.prependedTag != null) this.copy(appendedTag = null) else this.line

public fun TaggedLine.withoutPrependedTag(): Line
	= if (this.appendedTag != null) this.copy(prependedTag = null) else this.line

public object MidLevelParser {
	private const val ERR_MSG_EXCEPTION = "unhandled exception thrown"

	private const val ERR_MSG_UNEXPECTED_LLM_NODE = "unexpected low-level node type"

	private fun parseVoice(node: ParseTree): Lyric
		= when (node) {
			is AltVLyricContext -> parseVoice(node.findBareWordsInLyric()!!)
			is BareWordsInLyricContext -> node.parseTextAndContMarker().let { Lyric(it.first, it.second) }
			is MainVLyricContext -> parseVoice(node.findBareWordsInLyric()!!)
			else -> error(ERR_MSG_UNEXPECTED_LLM_NODE)
		}

	private fun parseVoices(node: LyricWithoutTagsContext): LyricWithoutTags {
		val nodeList = node.children!!.toMutableList()
		// low-level parsing doesn't inspect this deep, so there are terminal nodes (spaces) in here still
		val appendedAltVoice = (nodeList.last() as? AltVLyricContext)?.also { nodeList.removeLast() }?.let {
			if (nodeList.isEmpty()) return AltVoiceOnlyLyric(parseVoice(it))
			check(nodeList.removeLast().isSpaceToken) { ERR_MSG_UNEXPECTED_LLM_NODE }
			parseVoice(it)
		}
		val prependedAltVoice = (nodeList.first() as? AltVLyricContext)?.also { nodeList.removeFirst() }?.let {
			check(nodeList.removeFirst().isSpaceToken) { ERR_MSG_UNEXPECTED_LLM_NODE }
			parseVoice(it)
		}
		check(nodeList.size == 1) { "more nodes than expected in line (inner)" }
		return parseVoice(nodeList[0] as? MainVLyricContext ?: error(ERR_MSG_UNEXPECTED_LLM_NODE)).let {
			if (prependedAltVoice == null && appendedAltVoice == null) it else CombinedLyric(prependedAltVoice, it, appendedAltVoice)
		}
	}

	private fun parseLyrics(node: LyricWithoutTagsContext, prependedTag: LTag? = null, rTag: RTag? = null, appendedTag: LTag? = null): Line
		= parseVoices(node).let {
			if (rTag == null) it else RepeatedLine(rTag.repCount, it)
		}.let {
			if (prependedTag == null && appendedTag == null) it else TaggedLine(prependedTag, it, appendedTag)
		}

	private fun parseRTag(node: RTagContext): RTag
		= RTag(node.count!!.text!!.toUInt())

	private fun parseLTag(node: LTagContext): LTag
		= LTag(
			node.type!!.text!!,
			if (node.firstField == null) emptyList()
				else listOf(node.firstField!!.text) + (node.extraFields
					?.let { ctx -> ctx.children!!.windowed(4, 4).map { it[3].text } } // space, sep, space, bare words
					.orEmpty())
		)

	private fun parseLine(nodes: List<ParseTree>): Line {
		try {
			if (nodes.size == 1) return when (val singleNode = nodes[0]) {
				is RTagContext -> parseRTag(singleNode)
				is LTagContext -> parseLTag(singleNode)
				is LyricWithoutTagsContext -> parseLyrics(singleNode)
				else -> error(ERR_MSG_UNEXPECTED_LLM_NODE)
			}
			val nodeList = nodes.toMutableList()
			val prependedTag = if (nodeList.first() is LTagContext) parseLTag(nodeList.removeFirst() as LTagContext) else null
			val appendedTag = if (nodeList.last() is LTagContext) parseLTag(nodeList.removeLast() as LTagContext) else null
			val rTag = if (nodeList.last() is RTagContext) parseRTag(nodeList.removeLast() as RTagContext) else null
			val lyricNode = nodeList.singleOrNull() ?: error("more nodes than expected in line")
			check(lyricNode is LyricWithoutTagsContext) { ERR_MSG_UNEXPECTED_LLM_NODE }
			return parseLyrics(lyricNode, prependedTag, rTag, appendedTag)
		} catch (e: IllegalArgumentException) {
			throw e
		} catch (e: Exception) {
			throw Exception(ERR_MSG_EXCEPTION, e)
		}
	}

	public fun process(lowLevelParsed: List<List<List<ParseTree>>>): List<List<Line>>
		= lowLevelParsed.map { it.map(::parseLine) }
}

public data class NamedSnippetDeclaration(public val identifier: String, public val containedLines: List<Line>): LineOrSequence {
	public constructor(identifier: String, vararg lines: Line): this(identifier, lines.toList())

	init {
		require(identifier.isNotBlank())
		require(containedLines.isNotEmpty())
	}

	override val lineList: List<Line>
		get() = containedLines

	override fun reconstructSource(): String
		= "[${HighLevelParser.TAG_TYPE_SNIPPET_DECL_OPEN}: $identifier] ${containedLines.joinToString("\n") { it.reconstructSource() }} [${HighLevelParser.TAG_TYPE_REF_DECL_CLOSE}:]"

	override fun toString(): String
		= "NamedSnippetDeclaration(\n\t${identifier.asQuotedString},\n\t${containedLines.joinToString(",\n\t") { it.toString().increaseIndent }}\n)"

	public companion object
}

public sealed interface ParagraphOrSequence: HighLevelBranch {
	/** in source i.e. before unrolling refs/reps */
	public val paragraphList: List<Paragraph>

	public companion object
}

public sealed interface Paragraph: ParagraphOrSequence {
	override val paragraphList: List<Paragraph>
		get() = listOf(this)

	public companion object
}

public sealed interface ParagraphWithoutRepeat: Paragraph {
	public companion object
}

public data class ParagraphWithoutRepeatOrDecl(override val children: List<LineOrSequence>): ParagraphWithoutRepeat {
	public constructor(vararg children: LineOrSequence): this(children.toList())

	override fun reconstructSource(): String
		= children.joinToString("\n") { it.reconstructSource() }

	override fun toString(): String
		= "ParagraphWithoutRepeatOrDecl(\n\t${children.joinToString(",\n\t") { it.toString().increaseIndent }}\n)"

	public companion object
}

public data class NamedParagraphDeclaration(public val identifier: String, public val paragraph: ParagraphWithoutRepeatOrDecl): ParagraphWithoutRepeat {
	init {
		require(identifier.isNotBlank())
	}

	override val children: List<HighLevelBranchOrLeaf>
		get() = listOf(paragraph)

	override fun reconstructSource(): String
		= "[${HighLevelParser.TAG_TYPE_REF_DECL_OPEN}: $identifier]\n${paragraph.reconstructSource()}\n[${HighLevelParser.TAG_TYPE_REF_DECL_CLOSE}:]"

	override fun toString(): String
		= "NamedParagraphDeclaration(\n\t${identifier.asQuotedString},\n\t${paragraph.toString().increaseIndent}\n)"

	public companion object
}

public data class RepeatedParagraph(public val repCount: UInt, public val paragraph: ParagraphWithoutRepeat): Paragraph {
	init {
		require(repCount != 0U)
	}

	override val children: List<HighLevelBranchOrLeaf>
		get() = listOf(paragraph)

	override fun reconstructSource(): String
		= "${paragraph.reconstructSource()}\n[x$repCount]"

	override fun toString(): String
		= "RepeatedParagraph(\n\t$repCount,\n\t${paragraph.toString().increaseIndent}\n)"

	public companion object
}

public data class NamedMultiParagraphDeclaration(public val identifier: String, override val paragraphList: List<Paragraph>): ParagraphOrSequence {
	public constructor(identifier: String, vararg paragraphs: Paragraph): this(identifier, paragraphs.toList())

	override val children: List<HighLevelBranchOrLeaf>
		get() = paragraphList

	override fun reconstructSource(): String
		= "[${HighLevelParser.TAG_TYPE_REF_DECL_OPEN}: $identifier]\n${paragraphList.joinToString("\n\n") { it.reconstructSource() }}\n[${HighLevelParser.TAG_TYPE_REF_DECL_CLOSE}:]"

	override fun toString(): String
		= "NamedMultiParagraphDeclaration(\n\t${identifier.asQuotedString},\n\t${paragraphList.joinToString(",\n\t") { it.toString().increaseIndent }}\n)"

	public companion object
}

public data class SchemaInfo(public val version: String, public val convention: String? = null) {
	override fun toString(): String
		= "SchemaInfo(v$version${if (convention != null) ", $convention" else ""})"
}

public data class LyricFile(public val schemaInfo: SchemaInfo, override val children: List<ParagraphOrSequence>): HighLevelBranch {
	public constructor(schemaInfo: SchemaInfo, vararg children: ParagraphOrSequence): this(schemaInfo, children.toList())

	public val paragraphs: List<Paragraph> by lazy {
		children.flatMap(ParagraphOrSequence::paragraphList)
	}

	override fun reconstructSource(): String
		= paragraphs.joinToString("\n\n") { it.reconstructSource() }

	override fun toString(): String
		= "LyricFile(\n\t$schemaInfo,\n\t${children.joinToString(",\n\t") { it.toString().increaseIndent }}\n)"

	public companion object
}

public typealias WarningCallback = (lazyMessage: () -> Any) -> Unit

private const val PARSER_SCHEMA_VERSION: String = "0.8.0"

public object HighLevelParser {
	private const val PSEUDO_TAG_TYPE_REP = "x#"

	private val REGEX_SNIPPET_IDENTIFIER = Regex("""([A-Z][A-Z]?)([1-9][0-9]?)""")

	private val REGEX_TIMESTAMP = Regex("""(?:(?:[1-9][0-9]*:)?[0-5])?[0-9]:[0-5][0-9]""")

	private val ROMAN_NUMERALS_UNDER_100 = listOf(
		"",         "I",     "II",     "III",     "IV",     "V",     "VI",     "VII",     "VIII",     "IX",
		"X",       "XI",    "XII",    "XIII",    "XIV",    "XV",    "XVI",    "XVII",    "XVIII",    "XIX",
		"XX",     "XXI",   "XXII",   "XXIII",   "XXIV",   "XXV",   "XXVI",   "XXVII",   "XXVIII",   "XXIX",
		"XXX",   "XXXI",  "XXXII",  "XXXIII",  "XXXIV",  "XXXV",  "XXXVI",  "XXXVII",  "XXXVIII",  "XXXIX",
		"XL",     "XLI",   "XLII",   "XLIII",   "XLIV",   "XLV",   "XLVI",   "XLVII",   "XLVIII",   "XLIX",
		"L",       "LI",    "LII",    "LIII",    "LIV",    "LV",    "LVI",    "LVII",    "LVIII",    "LIX",
		"LX",     "LXI",   "LXII",   "LXIII",   "LXIV",   "LXV",   "LXVI",   "LXVII",   "LXVIII",   "LXIX",
		"LXX",   "LXXI",  "LXXII",  "LXXIII",  "LXXIV",  "LXXV",  "LXXVI",  "LXXVII",  "LXXVIII",  "LXXIX",
		"LXXX", "LXXXI", "LXXXII", "LXXXIII", "LXXXIV", "LXXXV", "LXXXVI", "LXXXVII", "LXXXVIII", "LXXXIX",
		"XC",     "XCI",   "XCII",   "XCIII",   "XCIV",   "XCV",   "XCVI",   "XCVII",   "XCVIII",   "XCIX",
	)

	internal const val TAG_TYPE_MOVEMENT = "mv"

	internal const val TAG_TYPE_REF = "rf"

	internal const val TAG_TYPE_REF_DECL_CLOSE = "cl"

	internal const val TAG_TYPE_REF_DECL_OPEN = "nr"

	internal const val TAG_TYPE_SCHEMA_INFO = "vr"

	internal const val TAG_TYPE_SNIPPET_DECL_OPEN = "ns"

	internal const val TAG_TYPE_SNIPPET_REF = "rs"

	internal const val ERR_FRAGMENT_MISSING_REF = "does not exist"

	internal const val ERR_FRAGMENT_REF_OUT_OF_SCOPE = "used before declaration"

	internal const val ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_FIELD_CONTENTS = "malformed well-known tag (fields did not pass validation)"

	internal const val ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_LOCATION = "well-known tag may not be used here"

	internal const val ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT = "invalid number of fields used for"

	internal const val ERR_MSG_CONT_MARK_IN_INVALID_POSITION = "cannot use enjambment/continuation mark here"

	internal const val ERR_MSG_MISSING_SCHEMA_INFO = "missing \"[$TAG_TYPE_SCHEMA_INFO:]\" (expected Circulyr source following v$PARSER_SCHEMA_VERSION schema)"

	internal const val ERR_MSG_NESTED_REF = "\"[$TAG_TYPE_REF:]\" may not be nested in \"[$TAG_TYPE_REF_DECL_OPEN:]\"/\"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\" (did you miss a \"[$TAG_TYPE_REF_DECL_CLOSE:]\"?)"

	internal const val ERR_MSG_NESTED_REF_DECL = "\"[$TAG_TYPE_REF_DECL_OPEN:]\" may not be nested in \"[$TAG_TYPE_REF_DECL_OPEN:]\"/\"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\" (did you miss a \"[$TAG_TYPE_REF_DECL_CLOSE:]\"?)"

	internal const val ERR_MSG_NESTED_SNIPPET_DECL = "\"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\" may not be nested in \"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\" (did you miss a \"[$TAG_TYPE_REF_DECL_CLOSE:]\"?)"

	internal const val ERR_MSG_NESTED_SNIPPET_REF = "\"[$TAG_TYPE_SNIPPET_REF:]\" may not be nested in \"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\" (did you miss a \"[$TAG_TYPE_REF_DECL_CLOSE:]\"?)"

	internal const val ERR_MSG_NOT_IN_REF_CANNOT_CLOSE = "cannot use lone \"[$TAG_TYPE_REF_DECL_CLOSE:]\" here (not in \"[$TAG_TYPE_REF_DECL_OPEN:]\", or in nested \"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\")"

	internal const val ERR_MSG_NOT_IN_SNIPPET_REF_CANNOT_CLOSE = "cannot use line-ending \"[$TAG_TYPE_REF_DECL_CLOSE:]\" here (not in \"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\")"

	internal const val ERR_MSG_PARA_BREAK_IN_SNIPPET_DECL = "paragraph breaks are not allowed in snippet declarations (\"[$TAG_TYPE_REF_DECL_OPEN:]\")"

	internal const val ERR_MSG_REDECLARATION_OF_NAMED_REF = "\"[$TAG_TYPE_REF_DECL_OPEN:]\" is a redeclaration of a prior named reference"

	internal const val ERR_MSG_REDECLARATION_OF_SNIPPET = "\"[$TAG_TYPE_REF_DECL_OPEN:]\" is a redeclaration of a prior snippet"

	internal const val ERR_MSG_REF_OR_DECL_DOES_NOT_SPAN_PARA = "\"[$TAG_TYPE_REF_DECL_OPEN:]\"/\"[$TAG_TYPE_REF:]\" must span the whole paragraph (except lone \"[$PSEUDO_TAG_TYPE_REP]\")"

	internal const val ERR_MSG_RTAG_APPLIED_TO_MULTI_PARA_REF = "\"[$PSEUDO_TAG_TYPE_REP]\" may only be used on \"[$TAG_TYPE_REF_DECL_OPEN:]\" when it contains a single paragraph"

	internal const val ERR_MSG_RTAG_APPLIES_TO_NOTHING = "lone \"[$PSEUDO_TAG_TYPE_REP]\" is not applied to a paragraph"

	internal const val ERR_MSG_RTAG_LINE_NOT_LAST_IN_PARA = "lone \"[$PSEUDO_TAG_TYPE_REP]\" must be the last line of a paragraph"

	internal const val ERR_MSG_UNCLOSED_REF_DECL = "reached EOF while in \"[$TAG_TYPE_REF_DECL_OPEN:]\"/\"[$TAG_TYPE_SNIPPET_DECL_OPEN:]\" (missing \"[$TAG_TYPE_REF_DECL_CLOSE:]\")"

	internal const val WARN_FRAGMENT_INCORRECT_SNIPPET_NUMBERING = "should be numbered sequentially from 1"

	internal const val WARN_FRAGMENT_MISMATCHED_SCHEMA_VER = "source file uses schema"

	internal const val WARN_FRAGMENT_UNUSED_DECLS = "are declared but never used"

	internal const val WARN_MSG_INCORRECT_MOVEMENT_NUMBERING = "should be numbered sequentially from I"

	internal const val WARN_MSG_LYRIC_IN_HEADER = "header paragraph should only contain tags"

	internal const val WARN_MSG_NO_CONVENTION_SPECIFIED = "no convention is specified"

	private enum class LTagLocation {
		LoneOwnPara,
		LoneAtParaStart,
		LoneAtParaMiddle,
		LoneAtParaEnd,
		AtLineStart,
		AtLineEnd,
	}

	private data class WellKnownLTag(
		val tagType: String,
		/** `null` => allow all */
		val allowedLocations: List<LTagLocation>?,
		val allowedFieldCount: IntRange,
		val extraPredicate: ((LTag) -> Boolean)? = null,
	) {
		fun doChecks(lTag: LTag, location: LTagLocation) {
			check(allowedLocations == null || location in allowedLocations) { "$ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_LOCATION: $lTag found in location $location" }
			check(lTag.fields.size in allowedFieldCount) { "$ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT [${lTag.tagType}:] (must be in $allowedFieldCount, got ${lTag.fields.size})" }
			check(extraPredicate?.invoke(lTag) != false) { "$ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_FIELD_CONTENTS: $lTag" }
		}

		companion object {
			@Suppress("MoveLambdaOutsideParentheses")
			val wellKnown = listOf(
				WellKnownLTag("br", listOf(LoneOwnPara), 0..0), // [br:]
				WellKnownLTag(TAG_TYPE_REF_DECL_CLOSE, listOf(LoneAtParaMiddle, LoneAtParaEnd, AtLineEnd), 0..0), // [cl:]
				WellKnownLTag("cm", allowedLocations = null, 0..1), // [cm: a comment]
				WellKnownLTag(TAG_TYPE_MOVEMENT, listOf(LoneOwnPara), 1..2, extraPredicate = { it.fields[0] in ROMAN_NUMERALS_UNDER_100 }), // [mv: I | Allegro]
				WellKnownLTag(TAG_TYPE_REF_DECL_OPEN, listOf(LoneAtParaStart), 1..1), // [nr: Chorus]
				WellKnownLTag(TAG_TYPE_SNIPPET_DECL_OPEN, listOf(AtLineStart), 1..1, extraPredicate = { REGEX_SNIPPET_IDENTIFIER in it.fields[0] }), // [ns: C1]
				WellKnownLTag(TAG_TYPE_REF, listOf(LoneOwnPara, LoneAtParaStart), 1..1), // [rf: Chorus]
				WellKnownLTag(TAG_TYPE_SNIPPET_REF, listOf(LoneOwnPara, LoneAtParaStart, LoneAtParaMiddle, LoneAtParaEnd), 1..1, extraPredicate = { REGEX_SNIPPET_IDENTIFIER in it.fields[0] }), // [rs: C1]
				WellKnownLTag("ts", listOf(LoneOwnPara, LoneAtParaStart), 1..1, extraPredicate = { REGEX_TIMESTAMP in it.fields[0] }), // [ts: 1:54]
				WellKnownLTag(TAG_TYPE_SCHEMA_INFO, listOf(LoneOwnPara, LoneAtParaStart), 1..2), // [vr: 0.8.0 | yoshi-1]
			).associateBy { it.tagType }
		}
	}

	private fun checkRefs(type: String, declared: Map<String, Int>, expected: List<Pair<String, Int>>, warnCallback: WarningCallback) {
		val unused = declared.keys - expected.map { (refID, refNotLineNumber) ->
			check((declared[refID] ?: error("$type $refID $ERR_FRAGMENT_MISSING_REF")) < refNotLineNumber) { "$type $refID $ERR_FRAGMENT_REF_OUT_OF_SCOPE" }
			refID
		}.toSet()
		if (unused.isNotEmpty()) warnCallback { "$type(s) $WARN_FRAGMENT_UNUSED_DECLS: ${unused.joinToString(", ") { it.asQuotedString }}" }
	}

	public fun process(midLevelParsed: List<List<Line>>, warnCallback: WarningCallback): LyricFile {
		fun checkContMark(contMark: ContinuationMarkOption, jambAllowed: Boolean, contAllowed: Boolean): Unit
			= check(when (contMark) {
				ContinuationMarkOption.Continuation -> contAllowed
				ContinuationMarkOption.Enjambment -> jambAllowed
				else -> true
			}) { ERR_MSG_CONT_MARK_IN_INVALID_POSITION }
		fun checkContMark(line: LineWithoutLTags, jambAllowed: Boolean, contAllowed: Boolean): Unit
			= when (line) {
				is AltVoiceOnlyLyric -> checkContMark(line.lyric, jambAllowed, contAllowed)
				is CombinedLyric -> {
					if (line.prependedAltVoice != null) checkContMark(line.prependedAltVoice.continuationMark, jambAllowed = jambAllowed && line.appendedAltVoice == null, contAllowed = contAllowed && line.appendedAltVoice == null)
					if (line.appendedAltVoice != null) checkContMark(line.appendedAltVoice, jambAllowed, contAllowed) // does not check following line/para has alt. voice
					checkContMark(line.mainVoice, jambAllowed, contAllowed)
				}
				is Lyric -> checkContMark(line.continuationMark, jambAllowed, contAllowed)
				is RepeatedLine -> checkContMark(line.line, jambAllowed, contAllowed = false)
			}
		fun checkContMark(line: LineWithoutLTags, isLastInPara: Boolean): Unit
			= checkContMark(line, jambAllowed = !isLastInPara, contAllowed = isLastInPara)
		fun checkLTagContent(lTag: LTag, location: LTagLocation) {
			WellKnownLTag.wellKnown[lTag.tagType]?.doChecks(lTag, location)
		}
		val lines = mutableListOf<Line>()
		midLevelParsed.forEach { para ->
			para.forEachIndexed { i, line ->
				when (line) {
					is LineWithoutLTags -> checkContMark(line, i == para.lastIndex)
					is LTag -> checkLTagContent(line, when (i) {
						0 -> if (para.size == 1) LoneOwnPara else LoneAtParaStart
						para.lastIndex -> LoneAtParaEnd
						else -> LoneAtParaMiddle
					})
					is TaggedLine -> {
						if (line.prependedTag != null) checkLTagContent(line.prependedTag, AtLineStart)
						if (line.appendedTag != null) checkLTagContent(line.appendedTag, AtLineEnd)
						checkContMark(line.line, i == para.lastIndex)
					}
					else -> Unit
				}
				lines += line
			}
			lines += ParagraphBreakMarker
		}
		val schemaInfo = lines.removeFirst().let { first ->
			check(first is LTag && first.tagType == TAG_TYPE_SCHEMA_INFO && first.fields.size in 1..2) { ERR_MSG_MISSING_SCHEMA_INFO }
			if (lines.getOrNull(0) == ParagraphBreakMarker) {
				lines.removeFirst()
			} else {
				val i = lines.indexOf(ParagraphBreakMarker)
				if (i != -1 && !lines.subList(0, i).all { it is LTag }) warnCallback { WARN_MSG_LYRIC_IN_HEADER }
			}
			SchemaInfo(first.fields[0], if (first.fields.size == 2) first.fields[1] else null.also { warnCallback { WARN_MSG_NO_CONVENTION_SPECIFIED } })
		}
		if (schemaInfo.version != PARSER_SCHEMA_VERSION) warnCallback { "$WARN_FRAGMENT_MISMATCHED_SCHEMA_VER v${schemaInfo.version}, it may not be parsed correctly" }

		val declaredMovements = mutableListOf<String>()
		val declaredRefs = mutableMapOf<String, Int>()
		val declaredSnippets = mutableMapOf<String, Int>()
		val expectedRefs = mutableListOf<Pair<String, Int>>()
		val expectedSnippets = mutableListOf<Pair<String, Int>>()
		var inRefDecl: String? = null // could have made this `stack: MutableList<String?>` but the max is 2 levels of nesting
		var inSnippetDecl: String? = null
		val processed = mutableListOf<ParagraphOrSequence>()
		val processing = mutableListOf<LineOrSequence>()
		val snippet = mutableListOf<Line>()
		val notLineCount = lines.size
		fun notLineNumber(): Int
			= notLineCount - lines.size
		while (true) {
			if (lines.isEmpty()) break // last line is always ParagraphBreakMarker, so the final paragraph will have been processed already when handling that
			val first = lines.removeFirst()
			when (first) {
				ParagraphBreakMarker -> {
					if (inSnippetDecl != null) {
						if (lines.isEmpty()) break // jump out of this loop and proceed to the assertion with a more helpful error message
						error(ERR_MSG_PARA_BREAK_IN_SNIPPET_DECL)
					}
					if (inRefDecl == null) {
						processed += when (processing.indexOfFirst { it is RTag }) {
							-1 -> ParagraphWithoutRepeatOrDecl(processing.toList())
							processing.lastIndex -> RepeatedParagraph((processing.removeLast() as RTag).repCount, ParagraphWithoutRepeatOrDecl(processing.toList()))
							else -> error(ERR_MSG_RTAG_LINE_NOT_LAST_IN_PARA)
						}
						processing.clear()
						continue
					}
				}
				is LineWithoutLTags -> Unit
				is LTag -> when (first.tagType) {
					TAG_TYPE_MOVEMENT -> {
						declaredMovements += first.fields[0] // field count/contents already validated
					}
					TAG_TYPE_REF -> {
						check(inRefDecl == null && inSnippetDecl == null) { ERR_MSG_NESTED_REF }
						check(processing.isEmpty() && (
							lines[0] == ParagraphBreakMarker
								|| (lines[0] is RTag && lines[1] == ParagraphBreakMarker)
						)) { ERR_MSG_REF_OR_DECL_DOES_NOT_SPAN_PARA }
						first.fields[0].let {
							expectedRefs += Pair(it, notLineNumber()) // field count/contents already validated
							processing += ReferenceTag(it)
						}
						continue
					}
					TAG_TYPE_REF_DECL_CLOSE -> {
						check(inRefDecl != null && inSnippetDecl == null) { ERR_MSG_NOT_IN_REF_CANNOT_CLOSE }
						// field count/contents already validated
						check(!declaredRefs.containsKey(inRefDecl)) { ERR_MSG_REDECLARATION_OF_NAMED_REF }
						val rTag = when (val nextLine = lines.removeFirst()) {
							ParagraphBreakMarker -> null
							is RTag -> nextLine.also {
								check(lines.removeFirst() == ParagraphBreakMarker) { ERR_MSG_RTAG_LINE_NOT_LAST_IN_PARA }
							}
							else -> error(ERR_MSG_REF_OR_DECL_DOES_NOT_SPAN_PARA)
						}
						val paras = mutableListOf<List<LineOrSequence>>()
						while (processing.isNotEmpty()) {
							val i = processing.indexOfFirst { it == ParagraphBreakMarker }
							if (i == -1) {
								paras += processing.toList()
								processing.clear()
								break
							}
							paras += processing.removeAt(0 until i)
							processing.removeAt(0)
						}
						processed += if (paras.size == 1) {
							NamedParagraphDeclaration(inRefDecl, ParagraphWithoutRepeatOrDecl(paras[0])).let { if (rTag != null) RepeatedParagraph(rTag.repCount, it) else it }
						} else {
							check(rTag == null) { ERR_MSG_RTAG_APPLIED_TO_MULTI_PARA_REF }
							NamedMultiParagraphDeclaration(inRefDecl, paras.map { ParagraphWithoutRepeatOrDecl(it) })
						}
						declaredRefs[inRefDecl] = notLineNumber()
						inRefDecl = null
						continue
					}
					TAG_TYPE_REF_DECL_OPEN -> {
						check(inRefDecl == null && inSnippetDecl == null) { ERR_MSG_NESTED_REF_DECL }
						check(processing.isEmpty()) { ERR_MSG_REF_OR_DECL_DOES_NOT_SPAN_PARA }
						inRefDecl = first.fields[0] // field count/contents already validated
						continue
					}
					TAG_TYPE_SNIPPET_REF -> {
						check(inSnippetDecl == null) { ERR_MSG_NESTED_SNIPPET_REF }
						first.fields[0].let {
							expectedSnippets += Pair(it, notLineNumber()) // field count/contents already validated
							processing += SnippetReferenceTag(it)
						}
						continue
					}
				}
				is RTag -> check(processing.isNotEmpty()) { ERR_MSG_RTAG_APPLIES_TO_NOTHING }
				is TaggedLine -> {
					if (first.prependedTag?.tagType == TAG_TYPE_SNIPPET_DECL_OPEN) {
						check(inSnippetDecl == null) { ERR_MSG_NESTED_SNIPPET_DECL }
						inSnippetDecl = first.prependedTag.fields[0] // field count/contents already validated
						lines.add(0, first.withoutPrependedTag())
						continue
					}
					if (first.appendedTag?.tagType == TAG_TYPE_REF_DECL_CLOSE) {
						check(inSnippetDecl != null) { ERR_MSG_NOT_IN_SNIPPET_REF_CANNOT_CLOSE }
						// field count/contents already validated
						check(!declaredSnippets.containsKey(inSnippetDecl)) { ERR_MSG_REDECLARATION_OF_SNIPPET }
						snippet += first.withoutAppendedTag()
						processing += NamedSnippetDeclaration(inSnippetDecl, snippet.toList())
						snippet.clear()
						declaredSnippets[inSnippetDecl] = notLineNumber()
						inSnippetDecl = null
						continue
					}
				}
			}
			if (inSnippetDecl != null) snippet += first
			else processing += first
		}
		check(inRefDecl == null && inSnippetDecl == null) { ERR_MSG_UNCLOSED_REF_DECL }
		checkRefs("named reference", declaredRefs.toMutableMap(), expectedRefs, warnCallback)
		checkRefs("snippet", declaredSnippets.toMutableMap(), expectedSnippets, warnCallback)
		declaredSnippets.keys.map { REGEX_SNIPPET_IDENTIFIER.matchEntire(it)!!.groupValues }
			.groupBy({ it[1] }, { it[2].toInt() })
			.mapValues { (_, subset) -> subset.withIndex().all { it.value == it.index + 1 } } // e.g. "V" subset of ["B1", "V1", "V2", "C1", "V3"] is [1, 2, 3] which is correct, so "V" is mapped to true
			.forEach { (prefix, isValid) -> if (!isValid) warnCallback { "snippets $prefix- $WARN_FRAGMENT_INCORRECT_SNIPPET_NUMBERING" } }
		if (!declaredMovements.withIndex().all { (i, s) -> ROMAN_NUMERALS_UNDER_100.indexOf(s) == i + 1 }) warnCallback { WARN_MSG_INCORRECT_MOVEMENT_NUMBERING }
		return LyricFile(schemaInfo, processed)
	}
}

@Suppress("MayBeConstant", "unused")
public val circulyrSchemaVersion: String = PARSER_SCHEMA_VERSION

@Suppress("UNUSED_PARAMETER")
public fun discardWarningsCallback(lazyMessage: () -> Any): Unit
	= Unit

public fun escalateWarningsToErrorsCallback(lazyMessage: () -> Any): Unit
	= error(lazyMessage())

public fun simpleWarnCallback(lazyMessage: () -> Any): Unit
	= println("warning: ${lazyMessage()}")

@Suppress("NOTHING_TO_INLINE")
public inline fun parseCirculyr(lowLevelParsed: List<List<List<ParseTree>>>, noinline warnCallback: WarningCallback = ::simpleWarnCallback): LyricFile
	= HighLevelParser.process(MidLevelParser.process(lowLevelParsed), warnCallback)

@Suppress("NOTHING_TO_INLINE")
public inline fun parseCirculyr(s: String, noinline warnCallback: WarningCallback = ::simpleWarnCallback): LyricFile
	= parseCirculyr(LowLevelParser.parse(s), warnCallback)
