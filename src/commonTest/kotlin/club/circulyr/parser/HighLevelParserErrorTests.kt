package club.circulyr.parser

import kotlin.test.Test
import kotlin.test.assertFails
import kotlin.test.assertTrue

public object HighLevelParserErrorTests {
	private fun assertDoesNotParse(msg: String, src: String, warningsAsErrors: Boolean = false): Throwable {
		val fSrc = src.trimMargin("@") + "\n"
		return if (warningsAsErrors) {
			parseCirculyr(fSrc, ::discardWarningsCallback) // check whether this test requires warnings as errors
			assertFails(msg) {
				println(parseCirculyr(fSrc, ::escalateWarningsToErrorsCallback)) // prints iff doesn't throw
			}
		} else {
			assertFails(msg) {
				println(parseCirculyr(fSrc, ::discardWarningsCallback)) // prints iff doesn't throw
			}
		}
	}

	private fun Throwable.assertMessageContains(fragment: String): Throwable
		= this.also {
			assertTrue(it.message?.contains(fragment) == true, "ex: $fragment; ac: ${it.message}")
		}

	@Test
	fun testMalformedDecls() {
		assertDoesNotParse(msg = "unclosed [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_UNCLOSED_REF_DECL)
		assertDoesNotParse(msg = "unclosed [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C1] A line
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_UNCLOSED_REF_DECL)
		assertDoesNotParse(msg = "[ns:] crosses paragraph boundary", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C1] A line
			@
			@A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_PARA_BREAK_IN_SNIPPET_DECL)
	}

	@Test
	fun testMalformedHeader() {
		assertDoesNotParse(msg = "first line is not [vr:]", """@A line
			@
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_MISSING_SCHEMA_INFO)
		assertDoesNotParse(msg = "mismatched schema version", """@[vr: 0.6.9 | yoshi-1]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_FRAGMENT_MISMATCHED_SCHEMA_VER)
		assertDoesNotParse(msg = "no convention specified", """@[vr: 0.8.0]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_MSG_NO_CONVENTION_SPECIFIED)
		assertDoesNotParse(msg = "lyric in header", """@[vr: 0.8.0 | yoshi-1]
			@A line
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_MSG_LYRIC_IN_HEADER)
	}

	@Test
	fun testMalformedWellKnownLTags() {
		val notEnough = "not enough fields in"
		val tooMany = "too many fields in"

		assertDoesNotParse(msg = "$tooMany [br:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[br: garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$tooMany [cm:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[cm: garbage | garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$notEnough [mv:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[mv:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
		assertDoesNotParse(msg = "invalid ordinal in [mv:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[mv: IIII]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_FIELD_CONTENTS)
		assertDoesNotParse(msg = "$tooMany [mv:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[mv: I | garbage | garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$notEnough [ts:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ts:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
		assertDoesNotParse(msg = "$tooMany [ts:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ts: 1:38 | garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$notEnough [vr:]", """@[vr:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
		assertDoesNotParse(msg = "$tooMany [vr:]", """@[vr: 0.8.0 | yoshi-1 | garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$notEnough [rf:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rf:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
		assertDoesNotParse(msg = "$tooMany [rf:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rf: garbage | garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$notEnough [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns:] A line
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
		assertDoesNotParse(msg = "invalid identifier in [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: garbage] A line
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_FIELD_CONTENTS)
		assertDoesNotParse(msg = "$tooMany [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: garbage | garbage] A line
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$notEnough [rs:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rs:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
		assertDoesNotParse(msg = "invalid identifier in [rs:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rs: garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_FIELD_CONTENTS)
		assertDoesNotParse(msg = "$tooMany [rs:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rs: garbage | garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$notEnough [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr:]
			@A line
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
		assertDoesNotParse(msg = "$tooMany [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: garbage | garbage]
			@A line
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)

		assertDoesNotParse(msg = "$tooMany [cl:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line
			@[cl: garbage]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_WRONG_FIELD_COUNT)
	}

	@Test
	fun testMiscWarnings() {
		assertDoesNotParse(msg = "unused [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@[cl:]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_FRAGMENT_UNUSED_DECLS)
		assertDoesNotParse(msg = "unused [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C1] A line [cl:]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_FRAGMENT_UNUSED_DECLS)

		assertDoesNotParse(msg = "incorrect numbering of [ns:] identifiers", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C2] A line [cl:]
			@
			@[rs: C2]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_FRAGMENT_INCORRECT_SNIPPET_NUMBERING)
		assertDoesNotParse(msg = "unordered numbering of [ns:] identifiers", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C2] A line [cl:]
			@[ns: C1] A line [cl:]
			@
			@[rs: C1]
			@[rs: C2]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_FRAGMENT_INCORRECT_SNIPPET_NUMBERING)

		assertDoesNotParse(msg = "incorrect numbering of [mv:] ordinals", """@[vr: 0.8.0 | yoshi-1]
			@
			@[mv: II]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_MSG_INCORRECT_MOVEMENT_NUMBERING)
		assertDoesNotParse(msg = "unordered numbering of [mv:] ordinals", """@[vr: 0.8.0 | yoshi-1]
			@
			@[mv: II]
			@
			@[mv: I]
		""", warningsAsErrors = true).assertMessageContains(HighLevelParser.WARN_MSG_INCORRECT_MOVEMENT_NUMBERING)
	}

	@Test
	fun testMisplacedContMarks() {
		val badJamb = "enjambment last in para,"
		val badCont = "continuation not last in para,"

		assertDoesNotParse(msg = "$badJamb main voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line /
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badCont main voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line...
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)

		assertDoesNotParse(msg = "$badJamb alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@(A line /)
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badCont alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@(A line...)
			@(A line)
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)

		assertDoesNotParse(msg = "$badJamb main voice w/ appended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line / (A line)
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badCont main voice w/ appended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line... (A line)
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)

		assertDoesNotParse(msg = "enjambment in prepended alt. voice w/ appended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@(A line /) A line (A line)
			@(A line)
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "continuation in prepended alt. voice w/ appended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@(A line...) A line (A line)
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badJamb prepended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@(A line /) A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badCont prepended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@(A line...) A line
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)

		assertDoesNotParse(msg = "$badJamb appended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line (A line /)
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badCont appended alt. voice", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line (A line...)
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)

		assertDoesNotParse(msg = "continuation w/ rTag", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line... [x2]
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badJamb w/ rTag", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line / [x2]
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
		assertDoesNotParse(msg = "$badCont w/ rTag", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line... [x2]
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_CONT_MARK_IN_INVALID_POSITION)
	}

	@Test
	fun testMisplacedRTags() {
		assertDoesNotParse(msg = "[x#] without paragraph", """@[vr: 0.8.0 | yoshi-1]
			@
			@[x2]
		""").assertMessageContains(HighLevelParser.ERR_MSG_RTAG_APPLIES_TO_NOTHING)
		assertDoesNotParse(msg = "[x#] not last in paragraph", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line
			@[x2]
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_RTAG_LINE_NOT_LAST_IN_PARA)
		assertDoesNotParse(msg = "[x#] applied to ref not last in paragraph", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@[cl:]
			@[x2]
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_RTAG_LINE_NOT_LAST_IN_PARA)
		assertDoesNotParse(msg = "[x#] applied to multi-para [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@
			@A line
			@[cl:]
			@[x2]
		""").assertMessageContains(HighLevelParser.ERR_MSG_RTAG_APPLIED_TO_MULTI_PARA_REF)
	}

	@Test
	fun testRefsAndDecls() {
		assertDoesNotParse(msg = "[nr:] is a redeclaration", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@[cl:]
			@
			@[nr: Chorus]
			@A line
			@[cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_REDECLARATION_OF_NAMED_REF)
		assertDoesNotParse(msg = "[ns:] is a redeclaration", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C1] A line [cl:]
			@[ns: C1] A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_REDECLARATION_OF_SNIPPET)

		assertDoesNotParse(msg = "[rf:] without [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rf: Chorus]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_MISSING_REF)
		assertDoesNotParse(msg = "[rf:] before [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rf: Chorus]
			@
			@[nr: Chorus]
			@A line
			@[cl:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_REF_OUT_OF_SCOPE)
		assertDoesNotParse(msg = "[rs:] without [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rs: C1]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_MISSING_REF)
		assertDoesNotParse(msg = "[rs:] before [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[rs: C1]
			@[ns: C1] A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_REF_OUT_OF_SCOPE)

		assertDoesNotParse(msg = "[nr:] not first line in paragraph", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line
			@[nr: Chorus]
			@A line
			@[cl:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_LOCATION)
		assertDoesNotParse(msg = "[rf:] not first line in paragraph", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@[cl:]
			@
			@A line
			@[rf: Chorus]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_LOCATION)
		assertDoesNotParse(msg = "[cl:] of [nr:] not last line in paragraph", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@[cl:]
			@A line
		""").assertMessageContains(HighLevelParser.ERR_MSG_REF_OR_DECL_DOES_NOT_SPAN_PARA)

		assertDoesNotParse(msg = "[nr:] in [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus 1]
			@A line
			@
			@[nr: Chorus 2]
			@A line
			@[cl:]
			@
			@A line
			@[cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_NESTED_REF_DECL)
		assertDoesNotParse(msg = "[ns:] in [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C1] A line
			@[ns: C2] A line
			@A line [cl:]
			@A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_NESTED_SNIPPET_DECL)
		assertDoesNotParse(msg = "[nr:] in [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C1] A line
			@[nr: Chorus 2]
			@A line
			@[cl:]
			@A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_LOCATION)
		assertDoesNotParse(msg = "[rf:] in [nr:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus 1]
			@A line
			@[cl:]
			@
			@[nr: Chorus 2]
			@A line
			@
			@[rf: Chorus 1]
			@
			@A line
			@[cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_NESTED_REF)
		assertDoesNotParse(msg = "[rf:] in [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus 1]
			@A line
			@[cl:]
			@
			@[ns: C2] A line
			@[rf: Chorus 1]
			@A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_FRAGMENT_WELL_KNOWN_LTAG_INVALID_LOCATION)
		assertDoesNotParse(msg = "[rs:] in [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[ns: C1] A line [cl:]
			@
			@[ns: C2] A line
			@[rs: C1]
			@A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_NESTED_SNIPPET_REF)

		assertDoesNotParse(msg = "closing tags swapped, breaking strict nesting", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@[ns: C1] A line
			@[cl:]
			@A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_NOT_IN_REF_CANNOT_CLOSE)
		assertDoesNotParse(msg = "[cl:] placed on next line, closing outer [nr:] instead of [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@[nr: Chorus]
			@A line
			@[ns: C1] A line
			@[cl:]
			@A line
			@[cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_NOT_IN_REF_CANNOT_CLOSE)
		assertDoesNotParse(msg = "[cl:] without [ns:]", """@[vr: 0.8.0 | yoshi-1]
			@
			@A line [cl:]
		""").assertMessageContains(HighLevelParser.ERR_MSG_NOT_IN_SNIPPET_REF_CANNOT_CLOSE)
	}
}
