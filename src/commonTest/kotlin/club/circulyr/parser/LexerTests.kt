package club.circulyr.parser

import club.circulyr.parser.CirculyrParser.Tokens
import club.circulyr.parser.CirculyrParser.Tokens.*
import org.antlr.v4.kotlinruntime.CharStreams
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.test.fail

public object LexerTests {
	private val String?.newlinesEscaped: String?
		get() = this?.replace("\n", "\\n")

	private val tokenLookup: Map<Int, Tokens> by lazy {
		Tokens.values().associateBy { it.id }
	}

	private fun standardText(type: Tokens): String?
		= when (type) {
			TAG_OPEN -> "["
			NEWLINE -> "\n"
			SPACE -> " "
			ALT_V_LYRIC_OPEN -> "("
			ALT_V_LYRIC_CLOSE -> ")"
			ENJAMBMENT_MARKER -> "/"
			CONTINUATION_MARKER -> "..."
			RTAG_CLOSE -> "]"
			TAG_TYPE_SEPARATOR -> ":"
			LTAG_CLOSE -> "]"
			LTAG_FIELD_SEPARATOR -> "|"
			SPACE_IN_LTAG -> " "
			else -> null
		}

	private data class TokenDesc(val type: Tokens, val text: String? = null)

	private fun String.assertTokenisesTo(expected: Array<TokenDesc>) {
		val acTokens = CirculyrLexer(CharStreams.fromString(this)).allTokens
		assertTrue(expected.size <= acTokens.size, "too many tokens (expect: ${expected.size}; actual: ${acTokens.size})")
		acTokens.forEachIndexed { i, token ->
			val (exType, exTextOrNull) = expected.getOrElse(i) { TokenDesc(BARE_WORD, "<additional token, not present in expect>") }
			val exText = exTextOrNull ?: standardText(exType)
			if (token.type != exType.id || token.text != exText) {
				fail("at ${token.line}:${token.charPositionInLine}, expected ${exType.id} ($exType) \"${exText.newlinesEscaped}\", got ${token.type} (${tokenLookup[token.type] ?: "<unknown>"}) \"${token.text.newlinesEscaped}\"")
			}
		}
	}

	@DslMarker
	private annotation class BuilderScopeDSLMarker

	@BuilderScopeDSLMarker
	private open class BuilderScope {
		open val bareWordType: Tokens = BARE_WORD

		open val space: TokenDesc = spaceInBareWords

		object InLTag: BuilderScope() {
			override val bareWordType: Tokens = BARE_WORD_IN_LTAG

			override val space: TokenDesc = spaceInLTag

			val sep: Array<TokenDesc> = arrayOf(space, TokenDesc(LTAG_FIELD_SEPARATOR), space)
		}
	}

	private fun build(block: (BuilderScope.() -> Array<TokenDesc>)): Array<TokenDesc>
		= block(BuilderScope())

	private fun <T: BuilderScope> T.bare(text: String): Array<TokenDesc>
		= text.split(' ').let { words ->
			Array(2 * words.size - 1) { if (it % 2 == 0) TokenDesc(this.bareWordType, words[it / 2]) else this.space }
		}

	private fun rTag(text: String): Array<TokenDesc>
		= arrayOf(TokenDesc(TAG_OPEN), TokenDesc(RTAG_TYPE, "x"), TokenDesc(REP_COUNT, text), TokenDesc(RTAG_CLOSE))

	private fun lTag(type: String, block: (BuilderScope.InLTag.() -> Array<TokenDesc>)? = null): Array<TokenDesc>
		= block?.invoke(BuilderScope.InLTag).orEmpty().let {
			buildList {
				add(tagOpen)
				add(TokenDesc(LTAG_TYPE, type))
				add(TokenDesc(TAG_TYPE_SEPARATOR))
				if (it.isNotEmpty()) {
					add(spaceInLTag)
					addAll(it)
				}
				add(lTagClose)
			}.toTypedArray()
		}

	@Suppress("NOTHING_TO_INLINE")
	private inline fun BuilderScope.altVBare(text: String): Array<TokenDesc>
		= arrayOf(altVOpen, *this.bare(text), altVClose)

	private val altVClose: TokenDesc = TokenDesc(ALT_V_LYRIC_CLOSE)

	private val altVOpen: TokenDesc = TokenDesc(ALT_V_LYRIC_OPEN)

	private val lTagClose: TokenDesc = TokenDesc(LTAG_CLOSE)

	private val tagOpen: TokenDesc = TokenDesc(TAG_OPEN)

	private val cont: TokenDesc = TokenDesc(CONTINUATION_MARKER)

	private val newline: TokenDesc = TokenDesc(NEWLINE)

	private val spaceInBareWords: TokenDesc = TokenDesc(SPACE)

	private val spaceInLTag: TokenDesc = TokenDesc(SPACE_IN_LTAG)

	private val jamb: Array<TokenDesc> = arrayOf(spaceInBareWords, TokenDesc(ENJAMBMENT_MARKER))

	@Test
	fun testEclipseBloodEnemies(): Unit
		= CirculyrTexts.eclipseBloodEnemies.assertTokenisesTo(build { arrayOf(
			*lTag("vr") { arrayOf(*bare("0.8.0"), *sep, *bare("yoshi-1")) }, newline,
			newline,
			*bare("Holy mother, the world's coming down"), newline,
			*bare("Payback strikes, the blood of the enemy"), newline,
			*bare("Father, it's falling apart"), newline,
			*bare("Raise your glass for the blood of the enemy"), newline,
			newline,
			*lTag("nr") { bare("Chorus 1") }, newline,
			*lTag("ns") { bare("C1") }, space, *bare("Like hell on wheels, ain't over 'til it's over"), newline,
			*bare("Revenge is sweet for us tonight"), space, *lTag("cl"), newline,
			*lTag("rs") { bare("C1") }, newline,
			*bare("Blood of the enemy"), newline,
			*lTag("cl"), newline,
			newline,
			*bare("Holy mother, let's wake up the dead"), newline,
			*bare("Judgment day, the blood of the enemy"), newline,
			*bare("Father, surrounded by flames"), newline,
			*bare("Burn it down, the blood of the enemy"), newline,
			newline,
			*lTag("rf") { bare("Chorus 1") }, newline,
			newline,
			*lTag("br"), newline,
			newline,
			*lTag("rs") { bare("C1") }, newline,
			*bare("'Cause we're tearing down the walls of lies, we can't pretend no more"), newline,
			*bare("So raise your glass this one last time"), newline,
			*bare("Blood of the enemies"), newline,
			newline,
			*bare("Give it all for the blood of the enemy"), space, *rTag("2"), newline,
			*bare("We want, we need the blood of the enemy"), space, *rTag("2"), newline,
		) })

	@Test
	fun testParamorePressure(): Unit
		= CirculyrTexts.paramorePressure.assertTokenisesTo(build { arrayOf(
			*lTag("vr") { arrayOf(*bare("0.8.0"), *sep, *bare("yoshi-1")) }, newline,
			newline,
			*bare("Tell me where our time went and if it was time well-spent"), newline,
			*bare("Just don't let me fall asleep feeling empty again"), newline,
			*bare("'Cause I fear I might break, and I fear I can't take it"), newline,
			*bare("Tonight I'll lie awake, feeling empty"), newline,
			newline,
			*lTag("nr") { bare("Chorus 1") }, newline,
			*lTag("ns") { bare("C1") }, space, *bare("I can feel the pressure, it's getting closer now"), newline,
			*bare("We're better off without you"), space, *lTag("cl"), newline,
			*lTag("rs") { bare("C1") }, newline,
			*lTag("cl"), newline,
			newline,
			*bare("Now that I'm losing hope, and there's nothing else to show"), *jamb, newline,
			*bare("For all of the days that we spent carried away from home"), newline,
			*lTag("ns") { bare("V1") }, space, *bare("Some things I'll never know, and I had to let them go"), newline,
			*bare("I'm sitting all alone, feeling empty"), space, *lTag("cl"), newline,
			newline,
			*lTag("rf") { bare("Chorus 1") }, newline,
			newline,
			*bare("Without you"), newline,
			newline,
			*bare("Some things I'll never know"), newline,
			*bare("And I had to let them go"), newline,
			*lTag("rs") { bare("V1") }, newline,
			newline,
			*lTag("rs") { bare("C1") }, newline,
			*bare("Feel the pressure, it's getting closer now"), newline,
			*bare("You're better off without me"), newline,
		) })

	@Test
	fun testScandroidAfterglowM(): Unit
		= CirculyrTexts.scandroidAfterglowM.assertTokenisesTo(build { arrayOf(
			*lTag("vr") { arrayOf(*bare("0.8.0"), *sep, *bare("yoshi-1")) }, newline,
			*lTag("cm") { bare("ready to publish") }, newline,
			newline,
			*lTag("mv") { bare("I") }, newline,
			newline,
			*altVBare("You and I will ride the afterglow"), space, *rTag("2"), newline,
			newline,
			*lTag("ts") { bare("1:38") }, newline,
			*lTag("ns") { bare("C1") }, space, *bare("In stereo we both let go"), space, *altVBare("In stereo we both let go"), newline,
			*bare("Into the flow, you and I will ride the afterglow"), space, *lTag("cl"), newline,
			newline,
			*altVBare("You and, you and I"), newline,
			*altVBare("Glow"), newline,
			*rTag("2"), newline,
		) })

	@Test
	fun testXandriaTheWatcher(): Unit
		= CirculyrTexts.xandriaTheWatcher.assertTokenisesTo(build { arrayOf(
			*lTag("vr") { arrayOf(*bare("0.8.0"), *sep, *bare("yoshi-1")) }, newline,
			newline,
			*lTag("ns") { bare("B1") }, space, *altVBare("Hate, greed, worlds turn to ashes"), newline,
			*altVBare("War, creed, no sign from heaven"), space, *lTag("cl"), newline,
			*altVBare("Burn, bleed, all just for nothing"), newline,
			newline,
			*bare("For centuries I've wandered here, across the ashes of mankind"), newline,
			*bare("The blood been spilled, the hopes deceived, there is no water turned to wine"), newline,
			*bare("A golden age, a woven maze of stolen strings and fake bliss"), newline,
			newline,
			*lTag("nr") { bare("Pre-chorus and Chorus 1") }, newline,
			*bare("Keep on bleeding, self-deceiving, all-or-nothing, make-believing"), *jamb, newline,
			*bare("That the winner will always be you"), newline,
			newline,
			*lTag("ns") { bare("C1") }, space, *bare("See the clouds in the sky still burning"), newline,
			*bare("Watch me counting the days in the rain"), space, *lTag("cl"), newline,
			*lTag("ns") { bare("C2") }, space, *bare("White eiderdown falls to the ground"), *jamb, newline,
			*bare("Covers all that has been with death"), space, *lTag("cl"), newline,
			*lTag("cl"), newline,
			newline,
			*lTag("rs") { bare("B1") }, newline,
			newline,
			*bare("So long ago, a flower rose, revealed its beauty and its thorns"), newline,
			*bare("We've covered those once-golden roads with blood of our children"), newline,
			newline,
			*lTag("rf") { bare("Pre-chorus and Chorus 1") }, newline,
			newline,
			*lTag("br"), newline,
			newline,
			*bare("Look and see what it could have been"), newline,
			*bare("But this all was just a dream"), cont, space, *altVBare("&&&&&&&&&&&&"), newline,
			newline,
			*bare("Just a dream of our foolish minds, so"), *jamb, newline,
			*lTag("rs") { bare("C1") }, newline,
			*bare("The nightfall has come so early"), *jamb, newline,
			*bare("Wiping all of our visions away"), newline,
			*lTag("rs") { bare("C2") }, newline,
			newline,
			*lTag("rs") { bare("B1") }, newline,
			*altVBare("Burn, bleed, all just for nothing"), newline,
			altVOpen, *bare("Wake up"), *jamb, altVClose, newline,
			*altVBare("See what you're becoming, see what we're becoming"), newline,
		) })
}
