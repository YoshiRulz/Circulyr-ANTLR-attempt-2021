package club.circulyr.parser

internal object CirculyrTexts {
	private val String.f: String
		get() = this.trimMargin("@") + "\n"

	val eclipseBloodEnemies: String = """@[vr: 0.8.0 | yoshi-1]
		@
		@Holy mother, the world's coming down
		@Payback strikes, the blood of the enemy
		@Father, it's falling apart
		@Raise your glass for the blood of the enemy
		@
		@[nr: Chorus 1]
		@[ns: C1] Like hell on wheels, ain't over 'til it's over
		@Revenge is sweet for us tonight [cl:]
		@[rs: C1]
		@Blood of the enemy
		@[cl:]
		@
		@Holy mother, let's wake up the dead
		@Judgment day, the blood of the enemy
		@Father, surrounded by flames
		@Burn it down, the blood of the enemy
		@
		@[rf: Chorus 1]
		@
		@[br:]
		@
		@[rs: C1]
		@'Cause we're tearing down the walls of lies, we can't pretend no more
		@So raise your glass this one last time
		@Blood of the enemies
		@
		@Give it all for the blood of the enemy [x2]
		@We want, we need the blood of the enemy [x2]
	""".f

	/** TODO */
	val keroKeroBonitoFlamingo: String = """@[vr: 0.8.0 | yoshi-1]
		@
		@[cm: TODO]
	""".f

	val paramorePressure: String = """@[vr: 0.8.0 | yoshi-1]
		@
		@Tell me where our time went and if it was time well-spent
		@Just don't let me fall asleep feeling empty again
		@'Cause I fear I might break, and I fear I can't take it
		@Tonight I'll lie awake, feeling empty
		@
		@[nr: Chorus 1]
		@[ns: C1] I can feel the pressure, it's getting closer now
		@We're better off without you [cl:]
		@[rs: C1]
		@[cl:]
		@
		@Now that I'm losing hope, and there's nothing else to show /
		@For all of the days that we spent carried away from home
		@[ns: V1] Some things I'll never know, and I had to let them go
		@I'm sitting all alone, feeling empty [cl:]
		@
		@[rf: Chorus 1]
		@
		@Without you
		@
		@Some things I'll never know
		@And I had to let them go
		@[rs: V1]
		@
		@[rs: C1]
		@Feel the pressure, it's getting closer now
		@You're better off without me
	""".f

	val scandroidAfterglowM: String = """@[vr: 0.8.0 | yoshi-1]
		@[cm: ready to publish]
		@
		@[mv: I]
		@
		@(You and I will ride the afterglow) [x2]
		@
		@[ts: 1:38]
		@[ns: C1] In stereo we both let go (In stereo we both let go)
		@Into the flow, you and I will ride the afterglow [cl:]
		@
		@(You and, you and I)
		@(Glow)
		@[x2]
	""".f

	val xandriaTheWatcher: String = """@[vr: 0.8.0 | yoshi-1]
		@
		@[ns: B1] (Hate, greed, worlds turn to ashes)
		@(War, creed, no sign from heaven) [cl:]
		@(Burn, bleed, all just for nothing)
		@
		@For centuries I've wandered here, across the ashes of mankind
		@The blood been spilled, the hopes deceived, there is no water turned to wine
		@A golden age, a woven maze of stolen strings and fake bliss
		@
		@[nr: Pre-chorus and Chorus 1]
		@Keep on bleeding, self-deceiving, all-or-nothing, make-believing /
		@That the winner will always be you
		@
		@[ns: C1] See the clouds in the sky still burning
		@Watch me counting the days in the rain [cl:]
		@[ns: C2] White eiderdown falls to the ground /
		@Covers all that has been with death [cl:]
		@[cl:]
		@
		@[rs: B1]
		@
		@So long ago, a flower rose, revealed its beauty and its thorns
		@We've covered those once-golden roads with blood of our children
		@
		@[rf: Pre-chorus and Chorus 1]
		@
		@[br:]
		@
		@Look and see what it could have been
		@But this all was just a dream... (&&&&&&&&&&&&)
		@
		@Just a dream of our foolish minds, so /
		@[rs: C1]
		@The nightfall has come so early /
		@Wiping all of our visions away
		@[rs: C2]
		@
		@[rs: B1]
		@(Burn, bleed, all just for nothing)
		@(Wake up /)
		@(See what you're becoming, see what we're becoming)
	""".f
}
