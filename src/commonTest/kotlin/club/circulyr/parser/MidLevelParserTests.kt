package club.circulyr.parser

import club.circulyr.parser.ContinuationMarkOption.Continuation
import club.circulyr.parser.ContinuationMarkOption.Enjambment
import kotlin.test.Test
import kotlin.test.assertEquals

public object MidLevelParserTests {
	@Suppress("NOTHING_TO_INLINE")
	private inline fun String.assertParsesTo(vararg expected: List<Line>): Unit
		= assertEquals(expected.toList(), MidLevelParser.process(LowLevelParser.parse(this)))

	@Test
	fun testEclipseBloodEnemies(): Unit
		= CirculyrTexts.eclipseBloodEnemies.assertParsesTo(
			listOf(
				LTag("vr", "0.8.0", "yoshi-1"),
			),
			listOf(
				Lyric("Holy mother, the world's coming down"),
				Lyric("Payback strikes, the blood of the enemy"),
				Lyric("Father, it's falling apart"),
				Lyric("Raise your glass for the blood of the enemy"),
			),
			listOf(
				LTag("nr", "Chorus 1"),
				TaggedLine(LTag("ns", "C1"), Lyric("Like hell on wheels, ain't over 'til it's over")),
				TaggedLine(Lyric("Revenge is sweet for us tonight"), LTag("cl")),
				LTag("rs", "C1"),
				Lyric("Blood of the enemy"),
				LTag("cl"),
			),
			listOf(
				Lyric("Holy mother, let's wake up the dead"),
				Lyric("Judgment day, the blood of the enemy"),
				Lyric("Father, surrounded by flames"),
				Lyric("Burn it down, the blood of the enemy"),
			),
			listOf(
				LTag("rf", "Chorus 1"),
			),
			listOf(
				LTag("br"),
			),
			listOf(
				LTag("rs", "C1"),
				Lyric("'Cause we're tearing down the walls of lies, we can't pretend no more"),
				Lyric("So raise your glass this one last time"),
				Lyric("Blood of the enemies"),
			),
			listOf(
				RepeatedLine(2U, Lyric("Give it all for the blood of the enemy")),
				RepeatedLine(2U, Lyric("We want, we need the blood of the enemy")),
			),
		)

	@Test
	fun testParamorePressure(): Unit
		= CirculyrTexts.paramorePressure.assertParsesTo(
			listOf(
				LTag("vr", "0.8.0", "yoshi-1"),
			),
			listOf(
				Lyric("Tell me where our time went and if it was time well-spent"),
				Lyric("Just don't let me fall asleep feeling empty again"),
				Lyric("'Cause I fear I might break, and I fear I can't take it"),
				Lyric("Tonight I'll lie awake, feeling empty"),
			),
			listOf(
				LTag("nr", "Chorus 1"),
				TaggedLine(LTag("ns", "C1"), Lyric("I can feel the pressure, it's getting closer now")),
				TaggedLine(Lyric("We're better off without you"), LTag("cl")),
				LTag("rs", "C1"),
				LTag("cl"),
			),
			listOf(
				Lyric("Now that I'm losing hope, and there's nothing else to show", Enjambment),
				Lyric("For all of the days that we spent carried away from home"),
				TaggedLine(LTag("ns", "V1"), Lyric("Some things I'll never know, and I had to let them go")),
				TaggedLine(Lyric("I'm sitting all alone, feeling empty"), LTag("cl")),
			),
			listOf(
				LTag("rf", "Chorus 1"),
			),
			listOf(
				Lyric("Without you"),
			),
			listOf(
				Lyric("Some things I'll never know"),
				Lyric("And I had to let them go"),
				LTag("rs", "V1"),
			),
			listOf(
				LTag("rs", "C1"),
				Lyric("Feel the pressure, it's getting closer now"),
				Lyric("You're better off without me"),
			),
		)

	@Test
	fun testScandroidAfterglowM(): Unit
		= CirculyrTexts.scandroidAfterglowM.assertParsesTo(
			listOf(
				LTag("vr", "0.8.0", "yoshi-1"),
				LTag("cm", "ready to publish"),
			),
			listOf(
				LTag("mv", "I"),
			),
			listOf(
				RepeatedLine(2U, AltVoiceOnlyLyric("You and I will ride the afterglow")),
			),
			listOf(
				LTag("ts", "1:38"),
				TaggedLine(LTag("ns", "C1"), CombinedLyric(prependedAltVoice = null, Lyric("In stereo we both let go"), Lyric("In stereo we both let go"))),
				TaggedLine(Lyric("Into the flow, you and I will ride the afterglow"), LTag("cl")),
			),
			listOf(
				AltVoiceOnlyLyric("You and, you and I"),
				AltVoiceOnlyLyric("Glow"),
				RTag(2U),
			),
		)

	@Test
	fun testXandriaTheWatcher(): Unit
		= CirculyrTexts.xandriaTheWatcher.assertParsesTo(
			listOf(
				LTag("vr", "0.8.0", "yoshi-1"),
			),
			listOf(
				TaggedLine(LTag("ns", "B1"), AltVoiceOnlyLyric("Hate, greed, worlds turn to ashes")),
				TaggedLine(AltVoiceOnlyLyric("War, creed, no sign from heaven"), LTag("cl")),
				AltVoiceOnlyLyric("Burn, bleed, all just for nothing"),
			),
			listOf(
				Lyric("For centuries I've wandered here, across the ashes of mankind"),
				Lyric("The blood been spilled, the hopes deceived, there is no water turned to wine"),
				Lyric("A golden age, a woven maze of stolen strings and fake bliss"),
			),
			listOf(
				LTag("nr", "Pre-chorus and Chorus 1"),
				Lyric("Keep on bleeding, self-deceiving, all-or-nothing, make-believing", Enjambment),
				Lyric("That the winner will always be you"),
			),
			listOf(
				TaggedLine(LTag("ns", "C1"), Lyric("See the clouds in the sky still burning")),
				TaggedLine(Lyric("Watch me counting the days in the rain"), LTag("cl")),
				TaggedLine(LTag("ns", "C2"), Lyric("White eiderdown falls to the ground", Enjambment)),
				TaggedLine(Lyric("Covers all that has been with death"), LTag("cl")),
				LTag("cl"),
			),
			listOf(
				LTag("rs", "B1"),
			),
			listOf(
				Lyric("So long ago, a flower rose, revealed its beauty and its thorns"),
				Lyric("We've covered those once-golden roads with blood of our children"),
			),
			listOf(
				LTag("rf", "Pre-chorus and Chorus 1"),
			),
			listOf(
				LTag("br"),
			),
			listOf(
				Lyric("Look and see what it could have been"),
				CombinedLyric(prependedAltVoice = null, Lyric("But this all was just a dream", Continuation), Lyric("&&&&&&&&&&&&")),
			),
			listOf(
				Lyric("Just a dream of our foolish minds, so", Enjambment),
				LTag("rs", "C1"),
				Lyric("The nightfall has come so early", Enjambment),
				Lyric("Wiping all of our visions away"),
				LTag("rs", "C2"),
			),
			listOf(
				LTag("rs", "B1"),
				AltVoiceOnlyLyric("Burn, bleed, all just for nothing"),
				AltVoiceOnlyLyric("Wake up", Enjambment),
				AltVoiceOnlyLyric("See what you're becoming, see what we're becoming"),
			),
		)
}
