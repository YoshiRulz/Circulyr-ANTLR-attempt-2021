package club.circulyr.parser

import club.circulyr.parser.CirculyrParser.*
import club.circulyr.parser.ContinuationMarkOption.*
import org.antlr.v4.kotlinruntime.tree.ParseTree
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

/** really not that useful but ¯\_(ツ)_/¯ */
public object LowLevelParserTests {
	private sealed interface LLMMatcher {
		val cat: String

		/** `null` => success, non-null string => failure */
		fun match(actual: ParseTree): String?
	}

	private sealed class LyricMatcherBase: LLMMatcher {
		/** `null` => success, non-null string => failure */
		abstract fun matchVoices(actual: List<ParseTree>): String?

		final override fun match(actual: ParseTree): String? {
			if (actual !is LyricWithoutTagsContext) return "node is not Lyric (w/o tags)"
			return matchVoices(buildList {
				actual.children!!.forEachIndexed { i, branch ->
					// add only the even-indexed nodes (the voices), omitting the spaces separating them
					when (i % 2) {
						0 -> add(branch)
						1 -> check(branch.isSpaceToken)
					}
				}
			})
		}

		companion object {
			fun ParseTree.matchesVoice(isAltV: Boolean, text: String, cont: ContinuationMarkOption): String? {
				val acBareWordsMaybeCont = if (isAltV) {
					if (this !is AltVLyricContext) return "node is not AltVLyric"
					(this.children!!.also { check(it.size == 3) })[1]
				} else {
					if (this !is MainVLyricContext) return "node is not MainVLyric"
					(this.children!!.also { check(it.size == 1) })[0]
				}
				if (acBareWordsMaybeCont !is BareWordsInLyricContext) return "wrong node type, expected bare words w/ any or no continuation mark"
				val (acText, acCont) = acBareWordsMaybeCont.parseTextAndContMarker()
				if (acText != text) return "contents differ (ex: ${text}; ac: ${acText})"
				if (acCont != cont) return "wrong continuation mark (ex: ${cont}; ac: ${acCont})"
				return null
			}
		}
	}

	private class AltVOnlyMatcher(private val text: String, private val cont: ContinuationMarkOption = None): LyricMatcherBase() {
		override val cat: String = "AltVOnly"

		override fun matchVoices(actual: List<ParseTree>): String? {
			if (actual.size != 1) return "wrong number of voices (ex: 1; ac: ${actual.size})"
			return actual[0].matchesVoice(isAltV = true, text, cont)
		}
	}

	private class LyricMatcher(
		private val mainVText: String,
		private val mainCont: ContinuationMarkOption = None,
		private val altVPostText: String? = null,
		private val altVPostCont: ContinuationMarkOption = None,
		private val altVPreText: String? = null,
	): LyricMatcherBase() {
		override val cat: String = "Lyric"

		override fun matchVoices(actual: List<ParseTree>): String? {
			val exHasPre = altVPreText != null
			val exHasPost = altVPostText != null
			val exVoiceCount = 1 + (if (exHasPre) 1 else 0) + (if (exHasPost) 1 else 0)
			if (actual.size != exVoiceCount) return "wrong number of voices (ex: $exVoiceCount; ac: ${actual.size})"
			var i = 0
			return (if (exHasPre) actual[i++].matchesVoice(isAltV = true, altVPreText!!, None) else null)
				?: actual[i++].matchesVoice(isAltV = false, mainVText, mainCont)
				?: (if (exHasPost) actual[i].matchesVoice(isAltV = true, altVPostText!!, altVPostCont) else null)
		}
	}

	private class RTagMatcher(private val repCount: String): LLMMatcher {
		override val cat: String = "RTag"

		override fun match(actual: ParseTree): String? {
			if (actual !is RTagContext) return "node is not RTag"
			val acRepCount = actual.count!!.text
			if (acRepCount != repCount) return "wrong rep count (ex: ${repCount}; ac: ${acRepCount})"
			return null
		}
	}

	private class LTagMatcher private constructor(
		private val type: String,
		private val fields: List<String>,
	): LLMMatcher {
		constructor(type: String, vararg fields: String): this(type, fields.toList())

		override val cat: String = "LTag"

		override fun match(actual: ParseTree): String? {
			if (actual !is LTagContext) return "node is not LTag"
			val acType = actual.type!!.text
			if (acType != type) return "wrong type (ex: $type; ac: $acType)"
			val acFields = actual.firstField?.text?.let { firstField ->
				listOf(firstField) + actual.extraFields?.children.orEmpty()
					.windowed(4, 4).map { it[3] as BareWordsInLTagContext } // space, sep, space, bare words
					.map { it.text }
			} ?: emptyList()
			if (acFields.size != fields.size) return "wrong number of fields (ex: ${fields.size}; ac: ${acFields.size})"
			acFields.forEachIndexed { i, acField ->
				if (acField != fields[i]) return "contents of field ${1 + i} differ (ex: ${fields[i]}; ac: ${acField})"
			}
			return null
		}
	}

	private fun String.assertParsesTo(vararg matchers: List<List<LLMMatcher>>) {
		val lowLevelParsed = LowLevelParser.parse(this)
		assertEquals(matchers.size, lowLevelParsed.size, "paragraphs count differs")
		lowLevelParsed.forEachIndexed { i, actualPara ->
			val expectPara = matchers.getOrElse(i) { emptyList() }
			assertEquals(expectPara.size, actualPara.size, "line count in paragraph $i differs")
			actualPara.forEachIndexed { j, actualLine ->
				val expectLine = expectPara.getOrElse(j) { emptyList() }
				assertEquals(expectLine.size, actualLine.size, "node count in line ${i}p+$j differs")
				actualLine.forEachIndexed { k, actualNode ->
					val matcher = expectLine.getOrNull(k)
					val result = if (matcher == null) "missing matcher"
						else matcher.match(actualNode)?.let { "[${matcher.cat}] $it" }
					if (result != null) fail("node (${i}p+$j):$k does not match: $result")
				}
			}
		}
	}

	@Test
	fun testEclipseBloodEnemies(): Unit
		= CirculyrTexts.eclipseBloodEnemies.assertParsesTo(
			listOf(
				listOf(LTagMatcher("vr", "0.8.0", "yoshi-1")),
			),
			listOf(
				listOf(LyricMatcher("Holy mother, the world's coming down")),
				listOf(LyricMatcher("Payback strikes, the blood of the enemy")),
				listOf(LyricMatcher("Father, it's falling apart")),
				listOf(LyricMatcher("Raise your glass for the blood of the enemy")),
			),
			listOf(
				listOf(LTagMatcher("nr", "Chorus 1")),
				listOf(LTagMatcher("ns", "C1"), LyricMatcher("Like hell on wheels, ain't over 'til it's over")),
				listOf(LyricMatcher("Revenge is sweet for us tonight"), LTagMatcher("cl")),
				listOf(LTagMatcher("rs", "C1")),
				listOf(LyricMatcher("Blood of the enemy")),
				listOf(LTagMatcher("cl")),
			),
			listOf(
				listOf(LyricMatcher("Holy mother, let's wake up the dead")),
				listOf(LyricMatcher("Judgment day, the blood of the enemy")),
				listOf(LyricMatcher("Father, surrounded by flames")),
				listOf(LyricMatcher("Burn it down, the blood of the enemy")),
			),
			listOf(
				listOf(LTagMatcher("rf", "Chorus 1")),
			),
			listOf(
				listOf(LTagMatcher("br")),
			),
			listOf(
				listOf(LTagMatcher("rs", "C1")),
				listOf(LyricMatcher("'Cause we're tearing down the walls of lies, we can't pretend no more")),
				listOf(LyricMatcher("So raise your glass this one last time")),
				listOf(LyricMatcher("Blood of the enemies")),
			),
			listOf(
				listOf(LyricMatcher("Give it all for the blood of the enemy"), RTagMatcher("2")),
				listOf(LyricMatcher("We want, we need the blood of the enemy"), RTagMatcher("2")),
			),
		)

	@Test
	fun testParamorePressure(): Unit
		= CirculyrTexts.paramorePressure.assertParsesTo(
			listOf(
				listOf(LTagMatcher("vr", "0.8.0", "yoshi-1")),
			),
			listOf(
				listOf(LyricMatcher("Tell me where our time went and if it was time well-spent")),
				listOf(LyricMatcher("Just don't let me fall asleep feeling empty again")),
				listOf(LyricMatcher("'Cause I fear I might break, and I fear I can't take it")),
				listOf(LyricMatcher("Tonight I'll lie awake, feeling empty")),
			),
			listOf(
				listOf(LTagMatcher("nr", "Chorus 1")),
				listOf(LTagMatcher("ns", "C1"), LyricMatcher("I can feel the pressure, it's getting closer now")),
				listOf(LyricMatcher("We're better off without you"), LTagMatcher("cl")),
				listOf(LTagMatcher("rs", "C1")),
				listOf(LTagMatcher("cl")),
			),
			listOf(
				listOf(LyricMatcher("Now that I'm losing hope, and there's nothing else to show", Enjambment)),
				listOf(LyricMatcher("For all of the days that we spent carried away from home")),
				listOf(LTagMatcher("ns", "V1"), LyricMatcher("Some things I'll never know, and I had to let them go")),
				listOf(LyricMatcher("I'm sitting all alone, feeling empty"), LTagMatcher("cl")),
			),
			listOf(
				listOf(LTagMatcher("rf", "Chorus 1")),
			),
			listOf(
				listOf(LyricMatcher("Without you")),
			),
			listOf(
				listOf(LyricMatcher("Some things I'll never know")),
				listOf(LyricMatcher("And I had to let them go")),
				listOf(LTagMatcher("rs", "V1")),
			),
			listOf(
				listOf(LTagMatcher("rs", "C1")),
				listOf(LyricMatcher("Feel the pressure, it's getting closer now")),
				listOf(LyricMatcher("You're better off without me")),
			),
		)

	@Test
	fun testScandroidAfterglowM(): Unit
		= CirculyrTexts.scandroidAfterglowM.assertParsesTo(
			listOf(
				listOf(LTagMatcher("vr", "0.8.0", "yoshi-1")),
				listOf(LTagMatcher("cm", "ready to publish")),
			),
			listOf(
				listOf(LTagMatcher("mv", "I")),
			),
			listOf(
				listOf(AltVOnlyMatcher("You and I will ride the afterglow"), RTagMatcher("2")),
			),
			listOf(
				listOf(LTagMatcher("ts", "1:38")),
				listOf(LTagMatcher("ns", "C1"), LyricMatcher("In stereo we both let go", altVPostText = "In stereo we both let go")),
				listOf(LyricMatcher("Into the flow, you and I will ride the afterglow"), LTagMatcher("cl")),
			),
			listOf(
				listOf(AltVOnlyMatcher("You and, you and I")),
				listOf(AltVOnlyMatcher("Glow")),
				listOf(RTagMatcher("2")),
			),
		)

	@Test
	fun testXandriaTheWatcher(): Unit
		= CirculyrTexts.xandriaTheWatcher.assertParsesTo(
			listOf(
				listOf(LTagMatcher("vr", "0.8.0", "yoshi-1")),
			),
			listOf(
				listOf(LTagMatcher("ns", "B1"), AltVOnlyMatcher("Hate, greed, worlds turn to ashes")),
				listOf(AltVOnlyMatcher("War, creed, no sign from heaven"), LTagMatcher("cl")),
				listOf(AltVOnlyMatcher("Burn, bleed, all just for nothing")),
			),
			listOf(
				listOf(LyricMatcher("For centuries I've wandered here, across the ashes of mankind")),
				listOf(LyricMatcher("The blood been spilled, the hopes deceived, there is no water turned to wine")),
				listOf(LyricMatcher("A golden age, a woven maze of stolen strings and fake bliss")),
			),
			listOf(
				listOf(LTagMatcher("nr", "Pre-chorus and Chorus 1")),
				listOf(LyricMatcher("Keep on bleeding, self-deceiving, all-or-nothing, make-believing", Enjambment)),
				listOf(LyricMatcher("That the winner will always be you")),
			),
			listOf(
				listOf(LTagMatcher("ns", "C1"), LyricMatcher("See the clouds in the sky still burning")),
				listOf(LyricMatcher("Watch me counting the days in the rain"), LTagMatcher("cl")),
				listOf(LTagMatcher("ns", "C2"), LyricMatcher("White eiderdown falls to the ground", Enjambment)),
				listOf(LyricMatcher("Covers all that has been with death"), LTagMatcher("cl")),
				listOf(LTagMatcher("cl")),
			),
			listOf(
				listOf(LTagMatcher("rs", "B1")),
			),
			listOf(
				listOf(LyricMatcher("So long ago, a flower rose, revealed its beauty and its thorns")),
				listOf(LyricMatcher("We've covered those once-golden roads with blood of our children")),
			),
			listOf(
				listOf(LTagMatcher("rf", "Pre-chorus and Chorus 1")),
			),
			listOf(
				listOf(LTagMatcher("br")),
			),
			listOf(
				listOf(LyricMatcher("Look and see what it could have been")),
				listOf(LyricMatcher("But this all was just a dream", Continuation, "&&&&&&&&&&&&")),
			),
			listOf(
				listOf(LyricMatcher("Just a dream of our foolish minds, so", Enjambment)),
				listOf(LTagMatcher("rs", "C1")),
				listOf(LyricMatcher("The nightfall has come so early", Enjambment)),
				listOf(LyricMatcher("Wiping all of our visions away")),
				listOf(LTagMatcher("rs", "C2")),
			),
			listOf(
				listOf(LTagMatcher("rs", "B1")),
				listOf(AltVOnlyMatcher("Burn, bleed, all just for nothing")),
				listOf(AltVOnlyMatcher("Wake up", Enjambment)),
				listOf(AltVOnlyMatcher("See what you're becoming, see what we're becoming")),
			),
		)
}
