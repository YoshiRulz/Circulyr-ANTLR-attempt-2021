package club.circulyr.parser

import club.circulyr.parser.ContinuationMarkOption.Continuation
import club.circulyr.parser.ContinuationMarkOption.Enjambment
import kotlin.test.Test
import kotlin.test.assertEquals

public object HighLevelParserTests {
	@Suppress("NOTHING_TO_INLINE")
	private inline fun String.assertParsesTo(expected: LyricFile): LyricFile
		= parseCirculyr(this, ::discardWarningsCallback).also { actual ->
			assertEquals(expected.reconstructSource(), actual.reconstructSource(), "serialisations differ")
			assertEquals(expected, actual, "ASTs differ")
		}

	@Test
	fun testEclipseBloodEnemies() {
		CirculyrTexts.eclipseBloodEnemies.assertParsesTo(LyricFile(
			SchemaInfo("0.8.0", "yoshi-1"),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Holy mother, the world's coming down"),
				Lyric("Payback strikes, the blood of the enemy"),
				Lyric("Father, it's falling apart"),
				Lyric("Raise your glass for the blood of the enemy"),
			),
			NamedParagraphDeclaration(
				"Chorus 1",
				ParagraphWithoutRepeatOrDecl(
					NamedSnippetDeclaration(
						"C1",
						Lyric("Like hell on wheels, ain't over 'til it's over"),
						Lyric("Revenge is sweet for us tonight"),
					),
					SnippetReferenceTag("C1"),
					Lyric("Blood of the enemy"),
				),
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Holy mother, let's wake up the dead"),
				Lyric("Judgment day, the blood of the enemy"),
				Lyric("Father, surrounded by flames"),
				Lyric("Burn it down, the blood of the enemy"),
			),
			ParagraphWithoutRepeatOrDecl(
				ReferenceTag("Chorus 1"),
			),
			ParagraphWithoutRepeatOrDecl(
				LTag("br"),
			),
			ParagraphWithoutRepeatOrDecl(
				SnippetReferenceTag("C1"),
				Lyric("'Cause we're tearing down the walls of lies, we can't pretend no more"),
				Lyric("So raise your glass this one last time"),
				Lyric("Blood of the enemies"),
			),
			ParagraphWithoutRepeatOrDecl(
				RepeatedLine(2U, Lyric("Give it all for the blood of the enemy")),
				RepeatedLine(2U, Lyric("We want, we need the blood of the enemy")),
			),
		))
	}

	@Test
	fun testParamorePressure() {
		CirculyrTexts.paramorePressure.assertParsesTo(LyricFile(
			SchemaInfo("0.8.0", "yoshi-1"),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Tell me where our time went and if it was time well-spent"),
				Lyric("Just don't let me fall asleep feeling empty again"),
				Lyric("'Cause I fear I might break, and I fear I can't take it"),
				Lyric("Tonight I'll lie awake, feeling empty"),
			),
			NamedParagraphDeclaration(
				"Chorus 1",
				ParagraphWithoutRepeatOrDecl(
					NamedSnippetDeclaration(
						"C1",
						Lyric("I can feel the pressure, it's getting closer now"),
						Lyric("We're better off without you"),
					),
					SnippetReferenceTag("C1"),
				)
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Now that I'm losing hope, and there's nothing else to show", Enjambment),
				Lyric("For all of the days that we spent carried away from home"),
				NamedSnippetDeclaration(
					"V1",
					Lyric("Some things I'll never know, and I had to let them go"),
					Lyric("I'm sitting all alone, feeling empty"),
				),
			),
			ParagraphWithoutRepeatOrDecl(
				ReferenceTag("Chorus 1"),
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Without you"),
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Some things I'll never know"),
				Lyric("And I had to let them go"),
				SnippetReferenceTag("V1"),
			),
			ParagraphWithoutRepeatOrDecl(
				SnippetReferenceTag("C1"),
				Lyric("Feel the pressure, it's getting closer now"),
				Lyric("You're better off without me"),
			),
		))
	}

	@Test
	fun testScandroidAfterglowM() {
		CirculyrTexts.scandroidAfterglowM.assertParsesTo(LyricFile(
			SchemaInfo("0.8.0", "yoshi-1"),
			ParagraphWithoutRepeatOrDecl(
				LTag("cm", "ready to publish"),
			),
			ParagraphWithoutRepeatOrDecl(
				LTag("mv", "I"),
			),
			ParagraphWithoutRepeatOrDecl(
				RepeatedLine(2U, AltVoiceOnlyLyric("You and I will ride the afterglow")),
			),
			ParagraphWithoutRepeatOrDecl(
				LTag("ts", "1:38"),
				NamedSnippetDeclaration(
					"C1",
					CombinedLyric(prependedAltVoice = null, Lyric("In stereo we both let go"), Lyric("In stereo we both let go")),
					Lyric("Into the flow, you and I will ride the afterglow"),
				),
			),
			RepeatedParagraph(
				2U,
				ParagraphWithoutRepeatOrDecl(
					AltVoiceOnlyLyric("You and, you and I"),
					AltVoiceOnlyLyric("Glow"),
				),
			),
		))
	}

	@Test
	fun testXandriaTheWatcher() {
		val actual = CirculyrTexts.xandriaTheWatcher.assertParsesTo(LyricFile(
			SchemaInfo("0.8.0", "yoshi-1"),
			ParagraphWithoutRepeatOrDecl(
				NamedSnippetDeclaration(
					"B1",
					AltVoiceOnlyLyric("Hate, greed, worlds turn to ashes"),
					AltVoiceOnlyLyric("War, creed, no sign from heaven"),
				),
				AltVoiceOnlyLyric("Burn, bleed, all just for nothing"),
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("For centuries I've wandered here, across the ashes of mankind"),
				Lyric("The blood been spilled, the hopes deceived, there is no water turned to wine"),
				Lyric("A golden age, a woven maze of stolen strings and fake bliss"),
			),
			NamedMultiParagraphDeclaration(
				"Pre-chorus and Chorus 1",
				ParagraphWithoutRepeatOrDecl(
					Lyric("Keep on bleeding, self-deceiving, all-or-nothing, make-believing", Enjambment),
					Lyric("That the winner will always be you"),
				),
				ParagraphWithoutRepeatOrDecl(
					NamedSnippetDeclaration(
						"C1",
						Lyric("See the clouds in the sky still burning"),
						Lyric("Watch me counting the days in the rain"),
					),
					NamedSnippetDeclaration(
						"C2",
						Lyric("White eiderdown falls to the ground", Enjambment),
						Lyric("Covers all that has been with death"),
					),
				),
			),
			ParagraphWithoutRepeatOrDecl(
				SnippetReferenceTag("B1"),
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("So long ago, a flower rose, revealed its beauty and its thorns"),
				Lyric("We've covered those once-golden roads with blood of our children"),
			),
			ParagraphWithoutRepeatOrDecl(
				ReferenceTag("Pre-chorus and Chorus 1"),
			),
			ParagraphWithoutRepeatOrDecl(
				LTag("br"),
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Look and see what it could have been"),
				CombinedLyric(prependedAltVoice = null, Lyric("But this all was just a dream", Continuation), Lyric("&&&&&&&&&&&&")),
			),
			ParagraphWithoutRepeatOrDecl(
				Lyric("Just a dream of our foolish minds, so", Enjambment),
				SnippetReferenceTag("C1"),
				Lyric("The nightfall has come so early", Enjambment),
				Lyric("Wiping all of our visions away"),
				SnippetReferenceTag("C2"),
			),
			ParagraphWithoutRepeatOrDecl(
				SnippetReferenceTag("B1"),
				AltVoiceOnlyLyric("Burn, bleed, all just for nothing"),
				AltVoiceOnlyLyric("Wake up", Enjambment),
				AltVoiceOnlyLyric("See what you're becoming, see what we're becoming"),
			),
		))
		assertEquals(11, actual.paragraphs.size) // should flatten multi-paragraph ref
	}
}
