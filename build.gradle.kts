@file:Suppress("SpellCheckingInspection", "UNUSED_VARIABLE")

import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
	`maven-publish`
	kotlin("multiplatform")
	id("com.github.ben-manes.versions") version "0.39.0"
}

kotlin {
	js(IR) {
		browser {
			testTask {
				useKarma {
					useFirefox()
				}
			}
		}
	}

	jvm {
		val javaTargetVersion = JavaLanguageVersion.of(16)
		jvmToolchain {
			(this as JavaToolchainSpec).languageVersion.set(javaTargetVersion)
		}
		compilations.all {
			kotlinOptions {
				jvmTarget = javaTargetVersion.toString()
			}
		}
		testRuns.all {
			executionTask.configure {
				useJUnitPlatform()
			}
		}
	}

//	linuxX64()

	explicitApi()

	sourceSets {
		fun creatingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = creating {
			parent?.let(::dependsOn)
			configuration(this)
		}

		fun gettingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = getting {
			parent?.let(::dependsOn)
			configuration(this)
		}

		fun depStr(partialCoords: String, version: String) = "$partialCoords:$version"

		fun kotlinx(module: String, version: String) = depStr("org.jetbrains.kotlinx:kotlinx-$module", version)

		fun ktANTLR(module: String) = depStr("com.strumenta.antlr-kotlin:antlr-kotlin-$module", "6304d5c1c4")

		val commonMain by gettingWithParent(null) {
			dependencies {
				api(ktANTLR("runtime"))
				implementation(project(":antlr-codegen"))
			}
		}

		val commonTest by gettingWithParent(null) {
			dependencies {
				implementation(kotlin("test"))
				implementation(kotlin("test-annotations-common"))
				implementation(kotlin("test-common"))
			}
		}

		val jsMain by gettingWithParent(commonMain)

		val jsTest by gettingWithParent(commonTest)

		val jvmMain by gettingWithParent(commonMain)

		val jvmTest by gettingWithParent(commonTest)

//		val linuxX64Main by gettingWithParent(commonMain)

//		val linuxX64Test by gettingWithParent(commonTest)

		all {
			kotlin.srcDir(name.takeLast(4).let { "src/${it.toLowerCase()}/${name.removeSuffix(it)}" })
			listOf(
				"kotlin.ExperimentalStdlibApi"
			).forEach(languageSettings::optIn)
		}
	}
}
